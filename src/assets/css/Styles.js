import {StyleSheet} from 'react-native';

export default Styles = StyleSheet.create({
    TTLErr:{
        color:'#fff',
        fontSize:16,
        fontFamily:'kurdishFont'
    }
});
export const ColorYellow='#ffd32a'
export const ColorBlack='#2f3640'
export const ColorIcon='#2f3640'
export const ColorNavy ='#013243'

export const PickerBg = [255, 118, 117, 1];
export const PickerToolBarBg = [214, 48, 49, 1];
export const PickerTitleColor = [255, 255, 255, 1];
export const PickerConfirmBtnColor = [255, 255, 255, 1];
export const PickerCancelBtnColor = [255, 255, 255, 1];
export const PickerFontColor = [0, 0, 0, 1];