import {StyleSheet, Dimensions, Platform} from 'react-native';
import {ColorYellow} from "./Styles";

const H = Dimensions.get("window").height
const W = Dimensions.get("window").width
export default styles = StyleSheet.create({
    MainView: {

        flex: 1,
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: "#fff"
    },

    SendText: {
        fontFamily: 'kurdishFont',
        color: '#fff',
        fontSize: 16

    },

    ChatText: {
        fontFamily: 'kurdishFont',
        // textAlign: 'right',
        fontSize: 16,
        color: '#333',
        margin: 3,
        borderRadius: 4

    },
    Input: {
        width: '86%',
        backgroundColor:ColorYellow,
        borderRadius: 8,
        fontFamily: 'kurdishFont',
        alignSelf: 'center',
        padding: 10,
        marginRight: 15

    },
    SendButton: {
        height: 50,
        width: 50,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 150,
        backgroundColor: '#22313f',
    },
    BottomSegment: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        alignSelf: 'center',
        margin: 10,

    },
    RenderTicketContainer:{
        marginHorizontal: 7,
        marginVertical: 4,
        borderRadius: 4,
        alignItems: 'center',
        padding: 8,
    }
});
