import {StyleSheet, Dimensions, Platform} from 'react-native';
import {ColorBlack, ColorNavy, ColorYellow} from "./Styles";

const H = Dimensions.get("window").height
const W = Dimensions.get("window").width
export default styles = StyleSheet.create({
    MainView: {
        flex: 1,
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: "#ecf0f1",
        // backgroundColor:'red'

    },
    TopImage: {
        backgroundColor: '#22313f',
        height: 250,
        width: '100%',
        resizeMode: 'contain',
        marginBottom: 20,
    },
    EachRowOrders: {
        width: '97%',
        overflow: 'hidden',
        marginVertical: 3,
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        borderRadius: 3,
        alignSelf: 'center'
    },
    Titr: {
        fontFamily: 'kurdishFont',
        margin: 3,
        fontSize: 15,
        color: ColorNavy,

    }, ChatListContainer: {
        // alignSelf:'center',
        // maxWidth: '100%',
        // alignItems:'center',

    },

    body: {
        fontFamily: 'kurdishFont',
        margin: 2,
        marginLeft: 19,
        fontSize: 14,
        color: ColorBlack,
        maxWidth: H > W ? 285 : W / 1.25,
        alignSelf: 'center'
    },
    DetailButtonText: {
        fontFamily: 'kurdishFont',
        marginVertical: 9,
        fontSize: 20,
        color: 'white'
    },
    EachRowHeader: {
        padding: 4,
        margin: 3,
        flexDirection: 'row-reverse',
        alignItems: 'center',
    },
    DetailButton: {
        marginVertical: 20,
        width: '60%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        height: 45,
        backgroundColor: ColorNavy,
        padding: 10,
        borderRadius: 4,
    }, map: {
        flex: .2,
        // ...StyleSheet.absoluteFillObject,
        width: '96%',
        height: 230,
        alignSelf: 'center',
        marginVertical: 15,
    },
    inp: {
        marginTop: 7,
        textAlign: 'center',
        fontFamily: "kurdishFont",
        width: "90%",
        color: "#808080",
        paddingLeft: 10,
        backgroundColor: '#f0f0f0',
        borderRadius: 4
    },

    ChatText: {            // for chat
        padding: 7,
        fontFamily: 'kurdishFont',
        // textAlign: 'right',
        fontSize: 16,
        color: '#333',
        margin: 3,
        borderRadius: 4

    },
    Input: {              // for chat
        width: '86%',
        backgroundColor: ColorYellow,
        borderRadius: 8,
        fontFamily: 'kurdishFont',
        alignSelf: 'center',
        padding: 10,
        marginRight: 15

    },
    SendButton: {          // for chat
        height: 50,
        width: 50,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 150,
        backgroundColor: '#22313f',
    },
    BottomSegment: {        // for chat
        marginTop: 30,
        // backgroundColor: '#ccc',
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        alignSelf: 'center',
        margin: 10,

    },
    ChatButton: {
        position: 'absolute',
        bottom: 8,
        left: 8,
        alignSelf: 'flex-end',
        height: 50,
        width: 50,
        borderRadius: 100,
        zIndex: 100,
        backgroundColor: ColorNavy,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ModalContainer: {
        zIndex: 200,
        flex: 1,
        top: 0,
        bottom: 0,
        right: 0,
        position: 'absolute'
    },
    ModalContainer2: {
        backgroundColor: 'rgba(46, 49, 49, .5)',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center'
    },
    ModalContainer3: {
        backgroundColor: '#fff',
        // height: '30%',
        width: '85%',
        padding: 15,
        alignItems: 'center',
        borderRadius: 4,
    },
    ModalBtnCont:
        {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
            width: '100%',
            marginVertical: 20
        },
    ChatContainer: {
        flex: 1,
        width: '100%',
        padding: 10
    }


});
