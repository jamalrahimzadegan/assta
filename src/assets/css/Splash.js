import {StyleSheet, Dimensions} from 'react-native';
import {ColorBlack, ColorYellow} from "./Styles";

export default styles = StyleSheet.create({
    mainsp: {
        width: "100%",
        flex: 1,
        backgroundColor: "#fff"
    },
    imgf: {
        width: 130,
        height: 130,
        resizeMode: "contain"
    },
    stltexte: {
        color: "#333",
        fontWeight: "bold",
        fontSize: 18,
        marginTop: 5,
    },
    langtext1: {
        color: "gray",
        fontWeight: "bold",
        fontSize: 16,
    },
    container: {
        width: "100%",
        height: Dimensions.get('window').height - 25,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#ffffff",
        resizeMode: "contain",
    },
    SplashUpdateBtn: {
        width: "90%",
        height: 40,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        backgroundColor: ColorBlack,
        marginTop: 12,
    },
    SplashUpdateTextBtn: {
        color: ColorYellow,
        fontFamily:'kurdishFont',
        fontSize: 15,
    },
    SplashUpText: {
        fontFamily:'kurdishFont',
        color: ColorBlack
    },
    UpdateOuter: {
        width: "100%",
        paddingVertical: 20,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 100,
    },
    languagesView: {
        width: "100%",
        height: 70,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        marginTop: 50,
    },
    langbtns: {
        width: 80,
        height: 40,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: ColorYellow,
        borderRadius: 4,
    },
    langtext: {
        color: ColorBlack,
        fontSize: 12,
        fontWeight: 'bold'
    },
});
