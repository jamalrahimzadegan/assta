import {StyleSheet, Dimensions, Platform} from 'react-native';
import {ColorBlack, ColorNavy, ColorYellow} from "./Styles";

const H = Dimensions.get("window").height
const W = Dimensions.get("window").width
export default styles = StyleSheet.create({
    MainView: {
        flex: 1,
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: '#ecf0f1',

    },
    MainButtonsContainer: {
        width: '90%',
        // alignSelf:'center',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    EachButton: {
        backgroundColor: ColorYellow,
        width: '50%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 10,
        marginBottom: 10,
        borderRadius: 4,
        paddingHorizontal: 15
    },
    text: {
        fontFamily: 'kurdishFont',
        fontSize: 14,
        color: ColorBlack,
        textAlign: 'right'
    },
    EachRowOrders: {
        ...Platform.select({
            android: {
                elevation: 15
            },
            ios: {
                shadowOffset: {width: 10, height: 10,},
                shadowColor: 'black',
                shadowOpacity: 1.0,
            }
        }),
        height: 120,
        flexShrink: 1,
        backgroundColor: '#fff',
        width: '96%',
        overflow: 'hidden',
        marginVertical: 4,
        marginHorizontal: 12,
        flexDirection: 'row-reverse',
        alignItems: 'center',
        padding: 10,
        borderRadius: 5,
        alignSelf: 'center'
    },
    OrderIMG: {
        resizeMode: 'cover',
        height: 90,
        width: 90,
        marginHorizontal: 2
    },
    Titr: {
        fontFamily: 'kurdishFont',
        fontSize: 13,
        color: ColorNavy
    },
    body: {
        fontFamily: 'kurdishFont',
        fontSize: 12,
        // lineHeight: 25,
        color: ColorBlack,
        flexShrink: 1,

    },
    EachRowHeader: {
        marginTop: 5,
        flexDirection: 'row-reverse',
        alignItems: 'center',
    },

    slide: {
        marginBottom: 25,
        backgroundColor: '#ecf0f1',
        width: W,
        height: H / 2.2,
        // marginVertical: 5,
    },
    TITR: {
        marginBottom: 5,
        borderRadius: 4,
        fontFamily: 'kurdishFont',
        textAlign: 'center',
        fontSize: 18,
        color: ColorBlack,
        backgroundColor: ColorYellow,
        paddingVertical: H > W ? 8 : 3,
        width: '96%',
        alignSelf: 'center',
    },
    Slider:{height: '100%', width: '100%', resizeMode: 'cover'},
    NewOrdersContainer:
        {paddingBottom: 10, backgroundColor: '#ecf0f1', height: H / 1.2}
});
