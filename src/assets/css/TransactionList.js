import {StyleSheet, Dimensions, Platform} from 'react-native';
import {ColorYellow} from "./Styles";


export default styles = StyleSheet.create({
    MainView: {
        flex: 1,
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: "#ffffff"
    },
    eachFactorText: {
        flexWrap: 'wrap',
        textAlign: 'right',
        color: 'black',
        fontFamily: 'kurdishFont',
        fontSize: 15,
        paddingVertical: 3
    },
    Container: {
        padding: 10
    },

    TansListContainer: {
        paddingVertical: 10,
        paddingBottom: 10,
        backgroundColor: '#ecf0f1'
    },
    RenderFactorsContainer:{
        ...Platform.select({
            ios:{
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.8,
                shadowRadius: 1,
            },
            android:{
                elevation:8,
            }
        }),
        backgroundColor:ColorYellow,
        margin:7,
        borderRadius:5
    }
});
