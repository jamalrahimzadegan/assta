import {AsyncStorage, StyleSheet} from "react-native";
import {ColorBlack, ColorYellow} from "./Styles";


export default styles = StyleSheet.create({
    DrawerContainer: {
        flex: 1,
        width: '100%',
        backgroundColor: ColorYellow,
    },
    Drawer: {
        flex: .8,
        width: '100%',
        paddingTop: 2,
        paddingBottom: 30,
        paddingHorizontal: 20,
    },
    NameText: {
        color: '#fff',
        borderRadius: 150,
        backgroundColor: ColorBlack,
        padding: 3,
        textAlign: 'center',
        fontFamily: 'kurdishFont',
        marginBottom: 10
    },
    text: {
        marginHorizontal: 10,
        fontFamily: 'kurdishFont',
        color: ColorBlack,
        fontSize: 15,
    },
    Logo: {
        alignSelf: 'center',
        resizeMode: 'cover',
        height: 180,
        width: 180,
        marginTop: 15,
        marginBottom: 30,
        borderRadius: 150
    },
    Details: {},
    Seperator: {
        width: '100%',
        height: 2,
        backgroundColor: 'black',
        marginVertical: 15,
        opacity: .2
    },
    ButtonContainer: {
        paddingVertical: 4,

    },
    EachButton: {
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 8,
    },
    DrawerLogo: {
        width: 150,
        height: 150,
        resizeMode: 'contain',
        alignSelf: 'center',
        opacity: .8,
    }
});
