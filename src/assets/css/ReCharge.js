import {Platform, StyleSheet} from 'react-native';
import {ColorNavy} from "./Styles";


export default styles = StyleSheet.create({
    containerx: {width: "100%", flex: 1, backgroundColor: '#fff'},
    part2: {width: "100%", flex: Platform.select({ios: 0.79, android: 0.84}),},
    con1: {width: "100%", flex: 1, alignItems: "center",},
    Vto2: {width: "100%", height: 50, justifyContent: "center", alignItems: "center", flexDirection: "row"},
    Vto: {width: "100%", height: 50, justifyContent: "center", alignItems: "center",},
    textViewe: {fontFamily: "kurdishFont", color: "gray", fontSize: 19,},
    textViewe2: {fontFamily: "kurdishFont", color: ColorNavy, fontSize: 22,},
    MainView: {width: "100%", justifyContent: "space-around", alignItems: "center", padding: 10, flexDirection: "row",},
    AllDRPD: {
        width: 150,
        padding: 10,
        height: 40,
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: ColorNavy,
    },
    btntxt: {fontFamily: "kurdishFont", color: "#ffffff", fontSize: 17, padding: 5},
    Input: {
        fontFamily: 'kurdishFont',
        fontSize: 15,
        borderColor: 'black',
        borderRadius: 5,
        borderWidth: 1.2,
        width: '50%',
        backgroundColor: '#fff',
        textAlign: 'center',
        height: 40
    },
    urlText: {color: '#333', borderColor: '#333', borderWidth: 1, margin: 5, padding: 10, borderRadius: 4,},
})
