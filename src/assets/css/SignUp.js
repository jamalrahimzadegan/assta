import {StyleSheet,Dimensions} from 'react-native';

export default styles = StyleSheet.create({
    MainView:{width:"100%",height:Dimensions.get("window").height,justifyContent:"center",alignItems:"center"
        ,backgroundColor:"#ffffff"},
    MainView3:{width:"100%",height:Dimensions.get("window").height,justifyContent:"center",alignItems:"center",backgroundColor:"#ffffff"},
    MainView1:{width:"70%",height:43,justifyContent:"center",alignItems:"center",alignSelf:"center",
        borderRadius:45,overflow:"hidden",borderColor:"#d3d3d3",borderWidth:2,flexDirection:"row"},
    loginBtn:{width:220,height:40,justifyContent:"center",alignItems:"center",borderRadius:5,backgroundColor:"#22313f",
        marginTop:30,},
    btntext:{fontFamily:"kurdishFont",fontSize:15,color:"#ffffff"},
    inp:{width:"90%",height:"100%",color:"#808080",paddingLeft:10,},
});
