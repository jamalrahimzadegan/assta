import {StyleSheet, Dimensions, Platform} from 'react-native';
import {ColorBlack, ColorNavy, ColorYellow} from "./Styles";

const H = Dimensions.get("window").height
const W = Dimensions.get("window").width
export default styles = StyleSheet.create({
    MainView: {
        flex: 1,
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: "#fff"
    },
    OrderIMG: {
        resizeMode: 'cover',
        height: 90,
        width: 90,
        marginHorizontal: 2
    },
    TITR: {
        fontFamily: 'kurdishFont',
        textAlign: 'center',
        fontSize: 18,
        color: ColorBlack,
        backgroundColor: ColorYellow,
        paddingVertical: H > W ? 10 : 9,
        width: '100%',

    },
    EachRowOrders: {
        ...Platform.select({
            android:{
                elevation:15
            },
            ios:{
                shadowOffset:{  width: 10,  height: 10,  },
                shadowColor: 'black',
                shadowOpacity: 1.0,
            }
        }),
        height:120,
        flexShrink:1,
        backgroundColor: '#fff',
        width: '96%',
        overflow: 'hidden',
        marginVertical: 5,
        marginHorizontal: 12,
        flexDirection: 'row-reverse',
        alignItems: 'center',
        padding: 10,
        borderRadius: 5,
        alignSelf: 'center'
    },
    Titr: {
        fontFamily: 'kurdishFont',
        fontSize: 13,
        color: ColorBlack,
    },
    body: {
        fontFamily: 'kurdishFont',
        fontSize: 13,
        maxWidth: H > W ? 200 : W / 1.65,
        color: ColorBlack
    },
    EachRowHeader: {
        flexDirection: 'row-reverse',
        // backgroundColor: 'red',
        width:'100%'
        // alignItems: 'center',
    },


});
