import {StyleSheet, Dimensions, Platform} from 'react-native';
import {ColorIcon, ColorYellow} from "./Styles";


export default styles = StyleSheet.create({
    NavigationContainer: {
        height: 50,
        padding: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        backgroundColor: ColorYellow
    },
    CallBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: ColorIcon,
        borderRadius: 100,
        borderWidth: 1.8,
        height: 38,
        width: 38,
        padding: 1,

    },
    iosStatus: {
        paddingTop: 25,
        backgroundColor: 'white',
        width: '100%',
        display: Platform.OS === 'ios' ? 'flex' : 'none'
    },
    ButtonText: {
        backgroundColor: '#ffcc00',
        color: '#333333',
        fontFamily: 'kurdishFont',
        padding: 7,
        fontSize: 15,
        borderRadius: 4
    },
    Badge: {
        color: 'red',
        fontFamily: 'kurdishFont',
        position: 'absolute',
        fontSize: 25,
        bottom: '20%',
        zIndex: 20
    },
});
