import {StyleSheet, Dimensions, Platform} from 'react-native';

const H = Dimensions.get("window").height
const W = Dimensions.get("window").width
export default styles = StyleSheet.create({
    MainView: {
        // ...Platform.select({
        //     ios: {
        //         paddingTop: 25
        //     }
        // }),
        flex: 1,
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: "#ffffff"
    },
   
});