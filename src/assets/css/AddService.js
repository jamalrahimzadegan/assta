import {StyleSheet, Dimensions, Platform} from 'react-native';

const H = Dimensions.get("window").height
const W = Dimensions.get("window").width
export default styles = StyleSheet.create({
    MainView: {
        // ...Platform.select({
        //     ios: {
        //         paddingTop: 25
        //     }
        // }),
        flex: 1,
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: '#ecf0f1',
    },
    TopText: {
        fontFamily: 'kurdishFont',
        textAlign: 'center',
        fontSize: 20,
        color: 'white',
        paddingVertical: H > W ? 10 : 9,
        backgroundColor: '#22313f',
        width: '100%',
    },
    FlatListMain: {
        width: '96%',

    },
    SetButton: {
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: '#22313f',
        width: '90%',
        borderRadius: 4,
        padding: 8,
        alignSelf: 'center',
        margin: 6
    },
    Button: {
        backgroundColor: '#22313f',
        width: '90%',
        borderRadius: 4,
        padding: 8,
        alignSelf: 'center',
        margin: 6
    },
    PercentButton:{
        margin:2,
        backgroundColor:'#fff',
        width:'30%',
        borderWidth:1,
        borderRadius:2,
        borderColor:'#22313f',
        padding:4
    },
    PercentButtonText: {
        fontFamily: 'kurdishFont',
        fontSize: 14,
        color: '#333',
        textAlign: 'center',

    },
    SubContainer: {
        marginVertical: 10,
        width: '90%',
        overflow: 'hidden',
        alignSelf:'center',
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-around'
    },
    FlatlistTitr: {
        marginVertical: 10,
        fontFamily: 'kurdishFont',
        fontSize: 14,
        color: '#fff',

    },
    FlatlistBody: {
        fontFamily: 'kurdishFont',
        fontSize: 12,
        color: '#333'
    },
    Comment: {
        padding:5,
        fontFamily: 'kurdishFont',
        fontSize: 15,
        color: '#333',
        textAlign: 'center'
    },
    Seperator:{
        width:'100%',
        height: 1.4,
        marginVertical:5,
        backgroundColor:'black'
    }
});
