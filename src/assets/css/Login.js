import {StyleSheet, Dimensions} from 'react-native';

export default styles = StyleSheet.create({
    MainView: {
        width: "100%", height: Dimensions.get("window").height, justifyContent: "center", alignItems: "center"
        , backgroundColor: "#ffffff"
    },
    MainView3: {
        width: "100%",
        height: Dimensions.get("window").height,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#ffffff"
    },
    MainView1: {
        width: "70%", height: 43, justifyContent: "center", alignItems: "center", alignSelf: "center", marginTop: 12,
        borderRadius: 5, overflow: "hidden", borderColor: "#555", borderWidth: 2, flexDirection: "row"
    },
    MainView2: {
        width: "70%",
        height: 43,
        justifyContent: "space-between",
        alignItems: "center",
        alignSelf: "center",
        marginTop: 12,
        borderRadius: 5,
        overflow: "hidden",
        borderColor: "#22313f",
        borderWidth: 2,
        flexDirection: "row",
        paddingRight: 10
    },
    loginBtn: {
        width: 220,
        height: 40,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        backgroundColor: "#22313f",
        marginTop: 30,
    },
    btntextd: {color: "#ffffff", fontFamily: "kurdishFont", fontSize: 16,},
    stlbtnx: {
        width: "70%",
        height: 43,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        backgroundColor: "#22313f",
        marginTop: 20,
    },
    btntext: {fontFamily: "kurdishFont", fontSize: 15, color: "#ffffff"},
    inp: {fontFamily: "kurdishFont", width: "90%", height: "100%", color: "#808080", paddingLeft: 10,},
    inp2: {
        width: "85%",
        height: "100%",
        color: "#808080",
        paddingLeft: 10,
        textAlign: "right",
        alignItems: "flex-end",
        fontFamily: "kurdishFont"
    },
    RulesTitr: {
        fontFamily: 'kurdishFont',
        color: '#22313F',
        fontSize: 17,
        padding: 10,
        textAlign: 'center'
    },
    WebViewRules: {
        height: '90%',
        width: '100%',
        backgroundColor: 'transparent',
        position: 'absolute',
        right: 0
    },
    ConfirmButton: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: '#bbb',
        borderRadius: 4,
        margin: 10,
        width: '50%',
        height: 50,
        alignSelf: 'center'
    },
    Rules: {
        backgroundColor: '#6bb9f0',
        alignSelf: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '90%',
        height: '85%',
        borderRadius: 4,
        padding: 10,
    },
    AcceptRulesText:{
        fontFamily: 'kurdishFont',
        color: '#22313F',
        fontSize: 14,
    }
});
