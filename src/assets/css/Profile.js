import {StyleSheet, Dimensions, Platform} from 'react-native';
import {ColorNavy, ColorYellow} from "./Styles";

export default styles = StyleSheet.create({
    MainView: {
        flex: 1,
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: '#ecf0f1',

    },

    ProfilePhotoContainer:{
        height:300,
        width: '100%',
        alignItems:'center',
        justifyContent:'center',
    },
    ProfilePhotoBtn:{
        height:'100%',
        width: '100%',
        alignItems:'center',
        justifyContent:'center',
    },
    ProfilePhoto: {
        height: '100%',
        width: '100%',
        resizeMode: 'contain'
    },

    NameText: {
        fontFamily: 'kurdishFont',
        color: ColorNavy,
        fontSize: 23,
        textAlign:'center',
        flexShrink: 1,
        marginVertical:10
    },
    EachLine: {
        marginVertical: 10,
        width: '96%',
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        overflow: 'hidden',
    },
    seprator: {
        width: '96%',
        backgroundColor: 'black',
        height: 2,
        alignSelf: 'center',
        marginBottom: 20
    },
    Label: {
        fontFamily: 'kurdishFont',
        color: ColorNavy,
        fontSize: 14,
        flexWrap: 'wrap',
        flexShrink: 1

    },
    Input: {
        fontFamily: 'kurdishFont',
        fontSize: 16,
        borderRadius: 5,
        width: '72%',
        paddingHorizontal: 10,
        backgroundColor: ColorYellow
    },
    UpdateButton: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: ColorNavy,
        borderRadius: 3,
        width: '60%',
        alignSelf: 'center',
        padding: 7,
        height: 45,
        marginVertical: 20
    },
});
