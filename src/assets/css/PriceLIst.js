import {StyleSheet, Dimensions, Platform} from 'react-native';
import {ColorBlack, ColorYellow} from "./Styles";

export default styles = StyleSheet.create({
    MainView: {
        flex: 1,
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: '#ecf0f1',
    },
    ListTitr: {
        fontFamily: 'kurdishFont',
        color: ColorBlack,
        backgroundColor: ColorYellow,
        padding: 12,
        width: '96%',
        marginHorizontal: 10,
        marginVertical:6,
        alignSelf: 'center',
        borderRadius: 4,
        fontSize: 16,
    },
    PricesList: {
        fontFamily: 'kurdishFont',
        color: '#333',
        fontSize: 15,
        marginRight: 10
    },
    TinyImage: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
    },
    EachRow: {
        alignItems: 'center',
        backgroundColor: '#fff',
        marginLeft: 8,
        margin: 4,
        padding: 5,
        width: '90%',
        borderRadius: 3
    }
});
