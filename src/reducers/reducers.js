import {createStore} from 'redux';
import  {AsyncStorage} from "react-native";


//---------------------------------initilState----------------------------------------------------------------------------
export const initilState = {
    id: null,
    Language: null,
    f_direction: null,
    drawer_update: 0,
};
//---------------------------------reducers----------------------------------------------------------------------------
const reducer = (state = initilState, action) => {
    switch (action.type) {
        case 'setId':
            AsyncStorage.setItem('id', action.payload.id.toString());
            return {...state, id: action.payload.id};
        case 'setLang':
            return {
                ...state,
                Language: action.payload.Language,
                f_direction: action.payload.f_direction,
                text_align: action.payload.text_align,
            };
        case 'DrawerUpdateFn':
            return {...state, drawer_update:action.payload.dUp};
        case 'PURGE':
            AsyncStorage.multiRemove(['phone', 'id']);
            return {...state, id: null};
    }
    return state;
};
//---------------------------------store----------------------------------------------------------------------------
export const store = createStore(reducer);
