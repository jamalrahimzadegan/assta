import React, {Component} from 'react';
import {AsyncStorage,Easing,
    Animated} from 'react-native';
import {createStackNavigator, createDrawerNavigator} from "react-navigation";
import Login from './components/auth/Login';
import Main from "./components/main/Main";
import Details from "./components/main/Details";
import MyOrders from "./components/main/MyOrders";
import Splash from "./components/Splash";
import Profile from "./components/main/Profile";
import TransactionsList from "./components/main/TransactionsList";
import AddService from "./components/main/AddService";
import ContactUs from "./components/main/ContactUs";
import PriceList from "./components/main/PriceList";
import ReCharge from "./components/main/ReCharge";
import DrawerStyle from "./components/DrawerStyle";
import RenderComment from "./components/main/OrderComment";
import {Provider} from 'react-redux';
import {store} from "./reducers/reducers";


//---------First Navigator------------------------------------------------------------------------------------
const AppNavigator = createStackNavigator({
        Splash: {screen: Splash, navigationOptions: {header: null}},
        Login: {screen: Login, navigationOptions: {header: null}},
        Main: {screen: Main, navigationOptions: {header: null}},
        MyOrders: {screen: MyOrders, navigationOptions: {header: null}},
        PriceList: {screen: PriceList, navigationOptions: {header: null}},
        ContactUs: {screen: ContactUs, navigationOptions: {header: null}},
        Comment: {screen: RenderComment, navigationOptions: {header: null}},
        Details: {screen: Details, navigationOptions: {header: null}},
        Profile: {screen: Profile, navigationOptions: {header: null}},
        TransactionsList: {screen: TransactionsList, navigationOptions: {header: null}},
        AddService: {screen: AddService, navigationOptions: {header: null}},
        ReCharge: {screen: ReCharge, navigationOptions: {header: null}},
        // City: {screen: City, navigationOptions: {header: null}},
    },
    {
        // initialRouteName: 'Splash'
    });
//---------Drawer Navigator------------------------------------------------------------------------------------
const Drawer = createDrawerNavigator({
    Home: {screen: AppNavigator, navigationOptions: {header: null}},
}, {
    drawerWidth: 275,
    drawerPosition: 'right',
    contentComponent: ({navigation}) => (<DrawerStyle  navigation={navigation}/>),

});
//---------Drawer Navigator En------------------------------------------------------------------------------------
const DrawerEn = createDrawerNavigator({
    Home: {screen: AppNavigator, navigationOptions: {header: null}},
}, {
    drawerWidth: 270,
    drawerPosition: 'left',
    contentComponent: ({navigation}) => (<DrawerStyle navigation={navigation}/>),
});


export default class App extends Component {
    constructor(props) {
        super(props);
        this.Lang = '';
        AsyncStorage.getItem('language')
            .then((lan) => {
            if (lan) {
                this.Lang = lan;
                this.forceUpdate()
            }
        });
    }

    render() {
        if (this.Lang === 'En') {
            return (
                <Provider store={store}>
                    <DrawerEn/>
                </Provider>
            )
        } else {
            return (
                <Provider store={store}>
                    <Drawer/>
                </Provider>
            )
        }
    }
}



