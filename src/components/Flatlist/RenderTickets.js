import React from 'react';
import {Text, View} from "react-native";
import {ColorYellow} from "../../assets/css/Styles";
import Icon1 from "react-native-vector-icons/AntDesign";
import Icon2 from "react-native-vector-icons/MaterialCommunityIcons";
import styles from "../../assets/css/ContactUs";


export default class RenderTickets extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {Dict, item, Language, f_direction} = this.props;
        return (
            <View style={[styles.RenderTicketContainer, {
                backgroundColor: item.item.role == 'admin' ? ColorYellow : '#ccc',
                alignSelf: item.item.role !== 'admin' ? 'flex-end' : 'flex-start',
                flexDirection: f_direction,
            }]}>
                {
                    item.item.role === 'admin' ?
                        <Text
                            style={[styles.ChatText,]}> {Dict['admin']}{item.item.message}</Text> :
                        item.item.role === 'provider' ?
                            <Text
                                style={[styles.ChatText,]}> {item.item.message}</Text> : null
                }
            </View>
        )
    }
}
