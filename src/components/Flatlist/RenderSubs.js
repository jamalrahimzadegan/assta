import React from 'react';
import {View} from 'react-native';
import styles from "../../assets/css/AddService";
import CheckBox from 'react-native-check-box';
import {ColorYellow} from "../../assets/css/Styles";

var x = 0;
export default class RenderSubs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ID: '',
            isChecked: false,
        };
        this.isChecked = false;
    }


    componentWillMount() {
        let item = this.props.item.item;
        if (this.props.OnServices.includes(parseInt(item.id))) {
            if (!this.isChecked) {
                this.isChecked = true;
            }
        }
    }


    render() {
        let {Language, Dict} = this.props;
        let OnServices = this.props.OnServices;
        let item = this.props.item.item;
        return (
            <View style={[styles.SubContainer, {paddingHorizontal: 20}]}>
                <CheckBox
                    uncheckedCheckBoxColor={'#22313F'}
                    isIndeterminate={false}
                    style={{marginHorizontal: 10, alignItems: Language === 'En' ? 'flex-start' : 'flex-end'}}
                    leftTextStyle={{
                        display: Language !== 'En' ? 'flex' : 'none',
                        fontFamily: 'kurdishFont',
                        color: '#22313F',
                        fontSize: 13,
                    }}
                    rightTextStyle={{
                        display: Language === 'En' ? 'flex' : 'none',
                        fontFamily: 'kurdishFont',
                        color: '#22313F',
                        fontSize: 13,
                    }}
                    leftText={' ' + (Language == 'Fa' ? item.title : Language == 'Ar' ? item.titleAr : Language == 'Ku' ? item.titleKu : '')}
                    rightText={item.titleEn + ' '}
                    onClick={() => {
                        this.isChecked = !this.isChecked;
                        this.forceUpdate();
                        if (!this.isChecked) {
                            this.props._DelKey(parseInt(item.id))
                        } else {
                            this.props._SetKeys(parseInt(item.id));
                        }
                    }}
                    isChecked={this.isChecked}
                    checkedCheckBoxColor={ColorYellow}
                />
            </View>
        )
    }
}
