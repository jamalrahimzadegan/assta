import React from 'react';
import {
    View,
    TouchableOpacity,
    Text,
} from 'react-native';
import styles from "../../assets/css/Main";
import FastImage from "react-native-fast-image";
import {MediaSmall} from "../../core/Connect";

export default class RenderNewOrders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let {Dict, Language, f_direction} = this.props;
        let item = this.props.item;
        return (
            <View style={{}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Details', {
                    Data: item.item
                })} style={[styles.EachRowOrders, {flexDirection: f_direction}]}>
                    {
                        item.item.image !== '' || undefined || null ? <FastImage
                                source={{uri: MediaSmall + item.item.image}}
                                // source={{uri: item.item.image!==''||undefined||null ? MediaSmall + item.item.image : require('../../assets/Images/order.png')}}
                                borderRadius={1}
                                style={styles.OrderIMG}/> :
                            <FastImage source={require('../../assets/Images/order.png')}
                                       borderRadius={1}
                                       style={styles.OrderIMG}/>
                    }
                    <View style={{marginHorizontal: 15, width: '70%'}}>
                        <View
                            style={[styles.EachRowHeader, {flexDirection: f_direction}]}>
                            <Text style={styles.Titr}>{Dict['customer_name']}{item.item.CustomerName}</Text>
                        </View>
                        <View
                            style={[styles.EachRowHeader, {flexDirection: f_direction}]}>
                            <Text style={styles.Titr} numberOfLines={1}>{Dict['orderlist_type']}</Text>
                            <Text
                                style={styles.body}>{Language == 'Fa' ? item.item.serviceTitle : Language == 'En' ? item.item.serviceTitleEn : Language == 'Ar' ? item.item.serviceTitleAr : Language == 'Ku' ? item.item.serviceTitleKu : null}</Text>
                        </View>
                        <View
                            style={[styles.EachRowHeader, {flexDirection: f_direction}]}>
                            <Text style={styles.Titr}>{Dict['t_requestdetail_status']}</Text>
                            <Text
                                style={styles.body}>{Language == 'Fa' ? item.item.status : Language == 'En' ? item.item.statusEn : Language == 'Ar' ? item.item.statusAr : Language == 'Ku' ? item.item.statusKu : null}</Text>
                        </View>
                        <View
                            style={[styles.EachRowHeader, {flexDirection: f_direction}]}>
                            <Text style={styles.Titr}>{Dict['orderlist_address']}</Text>
                            <Text numberOfLines={1} style={styles.body}> {item.item.address} </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}
