import React from 'react';
import {
    FlatList,
    View,
    TouchableOpacity,
    Text,
    AsyncStorage,
} from 'react-native';
import styles from "../../assets/css/AddService";
import ListEmpty from "../ListEmpty";
import RenderSubs from "./RenderSubs";
import Icon from "react-native-vector-icons/AntDesign";

export default class RenderAllServices extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Show: false,
        };
    }


    render() {
        let {Dict, Language} = this.props;
        let item = this.props.item;
        return (
            <View style={styles.FlatListMain}>
                <TouchableOpacity
                    style={[styles.Button, {
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: Language == 'En' ? 'flex-start' : 'flex-end'
                    }]}
                    onPress={() => this.setState({Show: !this.state.Show})}>
                    <Text style={styles.FlatlistTitr}>
                        {Language === 'En' ? item.cat.titleEn : Language === 'Fa' ? item.cat.title : Language === 'Ar' ? item.cat.titleAr : Language === 'Ku' ? item.cat.titleKu : ''}
                    </Text>
                </TouchableOpacity>
                {
                    this.state.Show ?
                        <FlatList
                            renderItem={(item, index) => <RenderSubs
                                item={item}
                                Language={Language}
                                Dict={Dict}
                                OnServices={this.props.OnServices}
                                _SetKeys={this.props._SetKeys}
                                _DelKey={this.props._DelKey}
                                _SetValue={this.props._SetValue}
                            />}
                            disableVirtualization={true}
                            showsVerticalScrollIndicator={false}
                            data={item.services}
                            keyExtractor={(item, index) => index}
                            listKey={(index) => index}
                            ListEmptyComponent={() => <ListEmpty EmptyText={Dict['noitem']}
                                                                 BgColor={'transparent'}/>}/> : null
                }

            </View>
        )
    }
}
