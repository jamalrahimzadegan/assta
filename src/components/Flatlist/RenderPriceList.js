import React from 'react';
import {
    FlatList,
    View,
    Text,
} from 'react-native';
import styles from "../../assets/css/PriceLIst";
import ListEmpty from "../ListEmpty";
import RenderPrices from "./RenderPrices";

export default class RenderPriceList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


    render() {
        let {item,Dict,f_direction,Language}=this.props;
        return (
            <View>
                <Text style={[styles.ListTitr, {textAlign: this.AlignText}]}>
                    {Language == 'En' ? item.item.service.titleEn : Language == 'Fa' ? item.item.service.title : Language == 'Ar' ? item.item.service.titleAr : Language == 'Ku' ? item.item.service.titleKu : null}
                </Text>
                <FlatList
                    renderItem={(item) => <RenderPrices item={item} Dict={Dict} Language={Language}
                                                        f_direction={f_direction}/>}
                    showsVerticalScrollIndicator={false}
                    data={item.item.prices}
                    listKey={(item, index) => 'D' + index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
            </View>
        )
    }
}
