import React from 'react';
import {
    View,
    Text,
} from 'react-native';
import styles from "../../assets/css/Details";
import {ColorYellow} from "../../assets/css/Styles";

export default class RenderFactors extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let {Dict,item} = this.props;
        return (
            <View style={[styles.ChatListContainer, {}]}>
                <Text style={[styles.ChatText, {
                    display: item.item.role === 'admin' ? 'flex' : 'none',
                    backgroundColor: '#fff',
                }]}> {Dict['admin']}
                    {item.item.role === 'admin' ? item.item.message : null}</Text>
                <Text style={[styles.ChatText, {
                    display: item.item.role === 'provider' ? 'flex' : 'none',
                    backgroundColor: '#bdc3c7',
                }]}>{Dict['user']}
                    {item.item.role === 'provider' ? item.item.message : null}</Text>
                <Text style={[styles.ChatText, {
                    display: item.item.role === 'customer' ? 'flex' : 'none',
                    backgroundColor: ColorYellow,
                }]}> {Dict['customer']}
                    {item.item.role === 'customer' ? item.item.message : null}</Text>
            </View>
        )
    }
}
