import React from 'react';
import {
    View,
    Text,
} from 'react-native';
import styles from "../../assets/css/TransactionList";
import {Connect} from "../../core/Connect";

export default class RenderFactors extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let item = this.props;
        let {Dict, Language, f_direction} = this.props;
        return (
            <View style={[{ alignItems: Language === 'En' ? 'flex-start' : 'flex-end'},styles.RenderFactorsContainer]}>
                <View style={[styles.Container, {}]}>
                    <View style={{flexDirection: f_direction}}>
                        <Text style={styles.eachFactorText}>{Dict['fact_number']}</Text>
                        <Text style={styles.eachFactorText}>  {item.item.id}</Text>
                    </View>
                    <View style={{flexDirection: f_direction}}>
                        <Text style={[styles.eachFactorText, {
                            color: '#52b3d9',
                            display: item.item.AddCash ? 'flex' : 'none'
                        }]}>{Dict['trans_inmoney']} {Connect.FormatNumber(parseInt(item.item.AddCash))}  </Text>
                    </View>
                    <View style={{flexDirection: f_direction}}>
                        <Text style={[styles.eachFactorText, {
                            color: '#f15a22',
                            display: item.item.DeCash ? 'flex' : 'none'
                        }]}>{Dict['trans_decmoney']} {Connect.FormatNumber(parseInt(item.item.DeCash))}  </Text>
                    </View>
                    <View style={{flexDirection: f_direction}}>
                        <Text style={[styles.eachFactorText, {display: item.item.Refid ? 'flex' : 'none'}]}>
                            {Dict['tracking_number']} {item.item.Refid}</Text>
                    </View>
                    <View style={{flexDirection: f_direction}}>
                        <Text style={styles.eachFactorText}>{Dict['trans_desc']}</Text>
                        <Text style={[styles.eachFactorText, {
                            maxWidth: '82%',
                            textAlign: 'center'
                        }]}>{Language == 'Fa' ? item.item.desc : Language == 'En' ? item.item.descEn : Language == 'Ar' ? item.item.descAr : Language == 'Ku' ? item.item.descKu : null}</Text>
                    </View>
                </View>
            </View>
        )
    }
}
