import React from 'react';
import {
    FlatList,
    View,
    TouchableOpacity,
    Text,
    AsyncStorage, Image,
} from 'react-native';
import styles from "../../assets/css/PriceLIst";
import {Connect, MediaSmall} from "../../core/Connect";

export default class RenderPrices extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


    render() {
        let {item, Dict,Language,f_direction} = this.props;
        return (
            <View style={[styles.EachRow, {
                justifyContent: Language === 'En' ? 'flex-start' : 'flex-end',
                flexDirection: f_direction,
                alignSelf: Language === 'En' ? 'flex-end' : 'flex-start',
            }]}>
                <Text
                    style={styles.PricesList}>{Language == 'En' ? item.item.service.textEn : Language == 'Fa' ? item.item.service.textFa : Language == 'Ar' ? item.item.service.textAr : Language == 'Ku' ? item.item.service.textKu : null}
                    : {Connect.FormatNumber(Number(item.item.price))} </Text>
                <Image source={{uri: item.item.image ? MediaSmall + item.item.image : null}} style={styles.TinyImage}/>
            </View>
        )
    }
}
