import styles from "../assets/css/Header";
import Icon from "react-native-vector-icons/Ionicons";
import Icon1 from 'react-native-vector-icons/AntDesign';
import React, {Component} from 'react';
import {
    View,StatusBar,
    TouchableOpacity,
    Platform,
    AsyncStorage,
    Linking
} from 'react-native';
import {ColorIcon, ColorYellow} from "../assets/css/Styles";
import {connect} from "react-redux";
import {NavigationActions, StackActions} from 'react-navigation';


 class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: ''
        };
    }
    componentWillMount(){
        AsyncStorage.getItem('support_phone').then((phone) => {
            this.setState({phone})
        });
    }
    render() {
        return (
            <View style={{}}>
                <View style={styles.iosStatus}/>
                <StatusBar backgroundColor={ColorYellow} barStyle="dark-content"/>
                <View style={[styles.NavigationContainer,{flexDirection:this.props.f_direction}]}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name={"md-menu"} color={ColorIcon} size={40}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.CallBtn} onPress={() => this._Home()}>
                        <Icon1 name={"home"} color={ColorIcon} size={27} style={{}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.CallBtn} onPress={() => this._SupportCall()}>
                        <Icon name={"ios-call"} color={ColorIcon} size={27} style={{}}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Icon name={"md-arrow-round-back"} color={ColorIcon} size={31}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    //----Calling Support--------------------------------------------------------------------------------------
    _SupportCall = () => {
        let phoneNumber = '';
        if (Platform.OS === 'android') {
            phoneNumber = `tel:${this.state.phone}`;
        }
        else {
            phoneNumber = `telprompt:${this.state.phone}`;
        }
        Linking.openURL(phoneNumber);
    };

     _Home() {
         const resetAction = StackActions.reset({
             index: 0,
             key: null,
             actions: [
                 NavigationActions.navigate({routeName: 'Main', params: {resetOrder: 1}}),
             ],
         });
         this.props.navigation.dispatch(resetAction);
     }
 }
//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);