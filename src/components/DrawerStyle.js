import React, {Component} from 'react';
import {
    Share,
    Alert,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    AsyncStorage, StatusBar,
} from 'react-native';
import styles from '../assets/css/DrawerStyle';
import {NavigationActions, StackActions} from 'react-navigation';
import URLS from '../core/URLS';
import {Connect, Media} from '../core/Connect';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon6 from 'react-native-vector-icons/Entypo';
import {ColorBlack, ColorYellow} from '../assets/css/Styles';
// import { ColorBlack} from '../assets/css/Styles';
import Dic from '../core/Dic';
import {connect} from 'react-redux';
import FastImage from "react-native-fast-image";

class DrawerStyle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ID: '',
            ProfilePhoto: '',
            Name: '',
            Family: '',
            Cash: '',
            Rank: '',
            MoarefCode: '',
        };
        this.Language = '';
    }


    //-------componentWillMount----------------------------------------------------------------------------------
    componentWillMount() {
        AsyncStorage.multiGet(['token', 'language', 'phone'])
            .then((x) => {
                this.Language = x[1][1];
                this.Phone = x[2][1];
                this.forceUpdate();
                this._Main();
            });
    }

    //-------componentDidUpdate----------------------------------------------------------------------------------
    componentDidUpdate(prevProps, prevState) {
        if (this.props.drawer_update != 0) {
            // console.warn('drawer_update: ',this.props.drawer_update)
            this._Main();
            AsyncStorage.multiGet([
                'language',
                'phone',
            ]).then((x) => {
                this.Language = x[0][1];
                this.Phone = x[1][1];
                this.forceUpdate()
            });
            this.props.DrawerUpdateFn(0)
        }
    }

    render() {
        let Dict = Dic[this.props.Language ? this.props.Language : 'Ku'];
        return (
            <ScrollView showsVerticalScrollIndicator={false}
                        style={[styles.DrawerContainer]}>
                <StatusBar backgroundColor={ColorYellow} barStyle="dark-content"/>
                {/*--------Profile Photo------------------------------------------------------------------------------------*/}
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Profile')}>
                    {
                        this.state.ProfilePhoto ?
                            <FastImage
                                source={{uri: this.state.ProfilePhoto ? URLS.Media() + this.state.ProfilePhoto : null}}
                                style={[styles.Logo]}
                            /> :
                            <FastImage
                                source={require('../assets/Images/ContractorProfile.png')}
                                style={[styles.Logo]}
                            />
                    }
                </TouchableOpacity>
                {/*--------Container------------------------------------------------------------------------------------*/}
                <View style={[styles.Drawer, {}]}>
                    {/*--Phone an Name----------------------------------------*/}
                    <Text style={[styles.text,{textAlign:'center'}]}>{this.Phone}</Text>
                    <Text style={[styles.NameText, ]}>{this.state.Name + ' ' + this.state.Family}</Text>
                    {/*--Rank--------------------*/}
                    <View style={[{flexDirection: this.props.f_direction}, styles.Details]}>
                        <Text
                            style={[styles.text, {}]}>{Dict['rank']} {this.state.Rank ? Connect.FormatNumber(this.state.Rank) : '-'}</Text>
                    </View>
                    {/*--Cash-------------------------------------------------------------------------------------*/}
                    <View style={[{flexDirection: this.props.f_direction}, styles.Details]}>
                        <Text
                            style={[styles.text, {}]}>{Dict['cash']}: {this.state.Cash ? Connect.FormatNumber(this.state.Cash) : '-'}</Text>
                    </View>
                    {/*--Moaref--------------------*/}
                    <View style={[{flexDirection: this.props.f_direction,}, styles.Details]} activeOpacity={.5}>
                        <Text onPress={() => Share.share({message: this.state.MoarefCode})}
                              style={[styles.text, {color: ColorBlack}]}>
                            {Dict['moraef_code']}: {this.state.MoarefCode ? this.state.MoarefCode : '-'}</Text>
                    </View>
                    <View style={styles.Seperator}/>
                    {/*--------Profile------------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={[styles.EachButton, {
                        alignSelf: this.Language !== 'En' ? 'flex-end' : 'flex-start',
                        flexDirection: this.props.f_direction,
                    }]}
                                      onPress={() => this.props.navigation.navigate('Profile')}
                                      activeOpacity={.5}>
                        <Icon2 name={'user'} color={ColorBlack} size={30}/>
                        <Text style={styles.text}>{Dict['edit_profile']}</Text>
                    </TouchableOpacity>
                    {/*--------Transactions------------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={[styles.EachButton, {
                        alignSelf: this.Language !== 'En' ? 'flex-end' : 'flex-start',
                        flexDirection: this.props.f_direction
                    }]}
                                      onPress={() => this.props.navigation.navigate('PriceList')}>
                        <Icon3 name={'cash-usd'} color={ColorBlack} size={30}/>
                        <Text style={styles.text}>{Dict['pricelist']}</Text>
                    </TouchableOpacity>
                    {/*--------ContactUs------------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={[styles.EachButton, {
                        alignSelf: this.Language !== 'En' ? 'flex-end' : 'flex-start',
                        flexDirection: this.props.f_direction
                    }]}
                                      onPress={() => this.props.navigation.navigate('ContactUs')}>
                        <Icon name={'md-contacts'} color={ColorBlack} size={30}/>
                        <Text style={styles.text}>{Dict['feedback']}</Text>
                    </TouchableOpacity>
                    {/*--------Change Language------------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={[styles.EachButton, {
                        alignSelf: this.Language !== 'En' ? 'flex-end' : 'flex-start',
                        flexDirection: this.props.f_direction,
                    }]} onPress={() => this._ChangeLang()}>
                        <Icon6 name={'language'} color={ColorBlack} size={29}/>
                        <Text style={styles.text}>{Dict['change_language']}</Text>
                    </TouchableOpacity>
                    {/*--------logOut------------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={[styles.EachButton, {
                        alignSelf: this.Language !== 'En' ? 'flex-end' : 'flex-start',
                        flexDirection: this.props.f_direction,
                    }]} onPress={() => this._Logout(Dict)}>
                        <View style={{alignItems: 'flex-end'}}>
                            <Icon name={'md-exit'} color={ColorBlack} size={32}/>
                        </View>
                        <Text style={styles.text}>{Dict['t_profile_logout']}</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }

    //-------Logout from the app in drawer------------------------------------------------------------
    _Logout(Dict) {
        Alert.alert('', Dict['logout_text'],
            [
                {
                    text: Dict['t_main_cancel'],
                    onPress: () => null,
                    style: 'Cancel',
                },
                {
                    text: Dict['picker_confirm'],
                    onPress: () => {
                        this.props.PURGE();
                        const resetAction = StackActions.reset({
                            index: 0,
                            key: null,
                            actions: [
                                NavigationActions.navigate({
                                    routeName: 'Splash',
                                    params: {resetOrder: 1},
                                }),
                            ],
                        });
                        this.props.navigation.dispatch(resetAction);
                    },
                },
            ]);
    }


    //-------Refresh Drawer---------------------------------------------------------------------------
    _Main() {
        Connect.SendPRequest(URLS.Link() + "main", {userId: parseInt(this.props.id)})
            .then(res => {
                // console.warn('drawer is up: ',res)
                if (res) {
                    this.setState({
                            ProfilePhoto: res.profile.image,
                            Name: res.profile.fName,
                            Family: res.profile.lName,
                            Cash: res.profile.cash,
                            Rank: res.profile.rank,
                            MoarefCode: res.profile.moarefCode,
                        }, () => null,
                    );
                    this.props.DrawerUpdateFn(0)
                }
            })
    }

    //-------Navigatie to Home Screen (Main)---------------------------------------------------------------------------
    _ChangeLang() {
        AsyncStorage.multiRemove([
            'language',
            // 'align',
            // 'direction',
        ]).then(() => {
            const resetAction = StackActions.reset({
                index: 0,
                key: null,
                actions: [
                    NavigationActions.navigate({routeName: 'Splash', params: {resetOrder: 1}}),
                ],
            });
            this.props.navigation.dispatch(resetAction);
        });
    }

}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        drawer_update: state.drawer_update,
        f_direction: state.f_direction,
        text_align: state.text_align,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        PURGE: () => dispatch({type: 'PURGE'}),
        DrawerUpdateFn: (x) => dispatch({
            type: 'DrawerUpdateFn', payload: {dUp: x},
        }),


    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerStyle);
