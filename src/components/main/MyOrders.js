import React from 'react';
import {
    FlatList,
    View,
    Text,
} from 'react-native';
import styles from "../../assets/css/MyOrders";
import DropdownAlert from 'react-native-dropdownalert';
import Styles from "../../assets/css/Styles";
import {Connect} from "../../core/Connect";
import ListEmpty from "../ListEmpty";
import Header from "../Header";
import RenderNewOrders from "../Flatlist/RenderNewOrders";
import {connect} from "react-redux";
import Dic from "../../core/Dic";
import URLS from "../../core/URLS";

class MyOrders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            MyOrders: [],
            Empty: false
        };
    }

    componentDidMount() {
        this.props.navigation.addListener("willFocus", payload => {
            this._GetMyOrders();
        });
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.MainView}>
                <Header navigation={this.props.navigation}/>
                <Text style={styles.TITR}>{Dict['my_orders']}</Text>
                <FlatList
                    style={{backgroundColor: '#ecf0f1'}}
                    renderItem={(item) => <RenderNewOrders item={item} Dict={Dict} Language={this.props.Language}
                                                           navigation={this.props.navigation}
                                                           f_direction={this.props.f_direction}/>}
                    showsVerticalScrollIndicator={false}
                    data={this.state.MyOrders}
                    keyExtractor={(index, item) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['main_norequest']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }

    //----------------GetMyOrders-------------------------------------------------------------------------------------------------------------------------------
    _GetMyOrders() {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link()+"providerorders", {userId: parseInt(this.props.id)})
            .then(res => {
                // console.warn('My Orders:');
                // console.warn(res);
                if (res) {
                    this.setState({MyOrders: res, Empty: false});
                }
            }).catch((e) => {
            this.setState({Empty: false});
        });
    }
}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(MyOrders);