import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    ActivityIndicator,
    TextInput,
    FlatList,
    ScrollView,
} from 'react-native';
import styles from "../../assets/css/ContactUs";
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {ColorYellow} from "../../assets/css/Styles";
import {Connect} from "../../core/Connect";
import Header from "../Header";
import ListEmpty from "../ListEmpty";
import Icon2 from "react-native-vector-icons/MaterialCommunityIcons";
import {connect} from "react-redux";
import Dic from "../../core/Dic";
import URLS from "../../core/URLS";
import RenderTickets from "../Flatlist/RenderTickets";


class ContactUs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Messages: [],
            FeedbackText: '',
            Loading: false,
            Count: false,
            counter: 1
        };
    }

    componentWillMount() {
        this.props.navigation.addListener("willFocus", payload => {
            this._GetTicket(Dic[this.props.Language]);
        });
    }

    _StartGettingMessage = () => {
        this._StartGettingMessage = setInterval(() => {
            this._GetTicket(Dic[this.props.Language]);
            // console.warn('interval is working')
        }, 13000)
    };

    componentDidMount() {
        this._StartGettingMessage()
    }

    componentWillUnmount() {
        this.props.navigation.addListener("willBlur", payload => {
            clearInterval(this._StartGettingMessage)
        });
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.MainView}>
                <Header navigation={this.props.navigation}/>
                <ScrollView showsVerticalScrollIndicator={false}
                            style={{flex: 1, width: '100%'}}
                            ref={ref => this.scrollView = ref}
                            onContentSizeChange={(contentWidth, contentHeight) => {
                                if (this.state.Count == true) {
                                    // alert('scrool')
                                    this.scrollView.scrollToEnd({animated: false});
                                    setTimeout(() => {
                                        this.setState({Count: false})
                                    }, 500)
                                }
                            }}>
                    <FlatList
                        renderItem={(item) => <RenderTickets item={item} Dict={Dict}
                                                             Language={this.props.Language}
                                                             f_direction={this.props.f_direction}/>}
                        showsVerticalScrollIndicator={false}
                        data={this.state.Messages}
                        keyExtractor={(index, item) => index.toString()}
                        ListEmptyComponent={() => <ListEmpty
                            EmptyText={Dict['noitem']}
                            BgColor={'transparent'}/>}
                    />
                </ScrollView>
                <View style={styles.BottomSegment}>
                    <TextInput
                        ref={input => {
                            this.textInput = input
                        }}
                        value={this.state.FeedbackText}
                        onChangeText={(FeedbackText) => this.setState({FeedbackText})}
                        underlineColorAndroid={"transparent"}
                        multiline={true}
                        placeholder={' '}
                        style={styles.Input}
                    />
                    <TouchableOpacity style={styles.SendButton} onPress={() => this._SendTicket(Dict)}>
                        {
                            this.state.Loading ?
                                <ActivityIndicator color={ColorYellow} size={27}/> :
                                <Icon2 name={'send'} color={ColorYellow} size={30}/>
                        }
                    </TouchableOpacity>
                </View>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }


    //---------------------Getting Tickets --------------------------------------------------------------------------------------
    _GetTicket(Dict) {
        Connect.SendPRequest(URLS.Link() + "getticket", {userId: parseInt(this.props.id)})
            .then(res => {
                // console.warn('get Tickets: ');
                // console.warn(res);
                if (res) {
                    this.setState({
                        Messages: res
                    })
                }
            }).catch(() => {
            this.setState({Loading: false});
            this.dropdown.alertWithType('error', '', Dict['global_error']);
        });
    }

    //---------------------sending ticket--------------------------------------------------------------------------------------
    _SendTicket(Dict) {
        if (this.state.FeedbackText != '') {
            this.setState({Loading: true});
            Connect.SendPRequest(URLS.Link() + "sendticket", {
                userId: parseInt(this.props.id),
                message: this.state.FeedbackText,
                role: 'provider'
            })
                .then(res => {
                    // console.warn('Factors: ');
                    // console.warn('sendticket was: ' + res);
                    if (res) {
                        this._GetTicket(Dict);
                        this.textInput.clear();
                        this.setState({Loading: false, Count: true});
                    }
                }).catch(() => {
                this.setState({Loading: false});
                this.dropdown.alertWithType('error', '', Dict['global_error']);
            });
        } else {
            this.dropdown.alertWithType('error', '', Dict['emptychat']);
        }
    }

}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        drawer_update: state.drawer_update,
        f_direction: state.f_direction,
        text_align: state.text_align,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactUs);