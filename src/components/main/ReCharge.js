import {WebView} from 'react-native-webview';
import URLS from "../../core/URLS";
import React from 'react';
import {AsyncStorage,View,TouchableOpacity,Alert,TextInput, Text} from 'react-native';
import Header from "../Header";
import styles from "./../../assets/css/ReCharge";
import {connect} from "react-redux";
import Dic from "../../core/Dic";
 class ReCharge extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            URL: '',
            price: '',
            showWebView: "none",
            WebViewAddress: {uri: ""},
            showMain: "flex",
            Text: ''

        };
        this._ChangeUrl = this._ChangeUrl.bind(this);
        this._handleBackButtonClick = this._handleBackButtonClick.bind(this);
    }


    _handleBackButtonClick() {
        this.props.navigation.goBack();
        return true;
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.containerx}>

                <View style={[styles.con1, {display: this.state.showMain, zIndex: 1}]}>
                    <View style={{width: '100%'}}>
                        <Header navigation={this.props.navigation}/>
                    </View>
                    <View style={styles.Vto}>
                        <Text style={styles.textViewe}>{Dict['increase_title']}</Text>
                    </View>
                    <View style={styles.MainView}>
                        <TouchableOpacity style={styles.AllDRPD} onPress={() => this._Send1(Dict)}>
                            <Text style={styles.btntxt}>{Dict['increase_price']}</Text>
                        </TouchableOpacity>
                        <TextInput
                            value={this.state.price}
                            onChangeText={(price) => this.setState({price: price})}
                            underlineColorAndroid={"transparent"}
                            keyboardType={"number-pad"}
                            returnKeyType={"done"}
                            placeholder={this.state.price ? this.state.price : Dict['increase_button']}
                            style={styles.Input}
                        />
                    </View>
                </View>
                {/*<Text style={styles.urlText}>{this.state.URL}</Text>*/}
                <WebView
                    style={{width: "100%", height: "100%", display: this.state.showWebView, zIndex: 0}}
                    onNavigationStateChange={this._ChangeUrl.bind(this)}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    source={this.state.WebViewAddress}
                >
                </WebView>
            </View>
        );
    }

    _ChangeUrl(webViewState) {
        let Dict = Dic[this.props.Language];
        this.setState({URL: webViewState.url})
        if (webViewState.url.indexOf("Status=OK") !== -1) {
            this.setState({showWebView: "none", showMain: "flex", WebViewAddress: {uri: ""}});
            Alert.alert(Dict['ScucsessText'], "", [
                {text: Dict['t_main_confirm']}
            ]);
            this.props.navigation.replace('Main')
        } else if (webViewState.url.indexOf("Status=NOK") !== -1) {
            // alert('cancel');
            this.setState({showWebView: "none", showMain: "flex", WebViewAddress: {uri: ""}});
            Alert.alert(Dict['FailedText'], "", [
                {text: Dict['t_main_confirm']}
            ]);
            this.props.navigation.replace('Main')
        } else if (webViewState.url.indexOf("abort") !== -1) {
            // alert('abort');
            this.setState({showWebView: "none", showMain: "flex", WebViewAddress: {uri: ""}});
            Alert.alert(Dict['FailedText'], "", [
                {text: Dict['t_main_confirm']}
            ]);
            this.props.navigation.replace('Main')
        } else if (webViewState.url.indexOf("panel/addcash") !== -1) {
            // alert('abort');
            this.setState({showWebView: "none", showMain: "flex", WebViewAddress: {uri: ""}});
            Alert.alert(Dict['FailedText'], "", [
                {text: Dict['t_main_confirm']}
            ]);
            this.props.navigation.replace('Main')
        }

    }


    _Send1(Dict) {
        if (this.state.price === "") {
            Alert.alert("", Dict['EmptyValueText']);
        } else if (parseInt(this.state.price) < 5000) {
            Alert.alert("", Dict['UnderNormalText']);
        } else {
         if(this.props.id){
             this.setState({
                 WebViewAddress: {uri: URLS.Link() + "paymentadd?mablagh=" + this.state.price + "&id_user=" + this.props.id},
                 showWebView: "flex", showMain: "none",
             });
         }
        }
    }
}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}
export default connect(mapStateToProps, mapDispatchToProps)(ReCharge);