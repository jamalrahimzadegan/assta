import React from 'react';
import {
    FlatList,
    View,
    Text,
} from 'react-native';
import styles from "../../assets/css/PriceLIst";
import DropdownAlert from 'react-native-dropdownalert';
import Styles from "../../assets/css/Styles";
import {Connect, MediaSmall} from "../../core/Connect";
import Header from "../Header";
import ListEmpty from "../ListEmpty";
import {connect} from "react-redux";
import Dic from "../../core/Dic";
import RenderPriceList from "../Flatlist/RenderPriceList";
import URLS from "../../core/URLS";


class PriceList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ID: '',
            Empty: false,
            Prices: []
        };
    }

    componentWillMount() {
        this.props.navigation.addListener("willFocus", payload => {
            this._GetPriceList();
        });
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.MainView}>
                <Header navigation={this.props.navigation}/>
                <FlatList
                    renderItem={(item) => <RenderPriceList item={item} Dict={Dict} Language={this.props.Language}
                                                           f_direction={this.props.f_direction}/>}
                    showsVerticalScrollIndicator={false}
                    data={this.state.Prices}
                    keyExtractor={(index) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }

    //---------------------Getting Orders --------------------------------------------------------------------------------------
    _GetPriceList() {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link()+"providerprices", {userId: parseInt(this.props.id)})
            .then(res => {
                // console.warn('pirce list');
                // console.warn(res);
                if (res) {
                    this.setState({Empty: false, Prices: res});
                } else {
                }
            }).catch((e) => {
            this.setState({Empty: false});
        });
    }
}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(PriceList);