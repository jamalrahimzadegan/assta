import React from 'react';
import {
    View,
    FlatList,
    TextInput,
    TouchableOpacity,
    ActivityIndicator,
    ScrollView,
} from 'react-native';
import styles from "../../assets/css/Details";
import ListEmpty from "../ListEmpty";
import Header from "../Header";
import {Connect} from "../../core/Connect";
import Icon2 from "react-native-vector-icons/MaterialCommunityIcons";
import DropdownAlert from "react-native-dropdownalert";
import Styles, {ColorYellow} from "../../assets/css/Styles";
import URLS from "../../core/URLS";
import {connect} from "react-redux";
import Dic from "../../core/Dic";
import RenderOrderComment from "../Flatlist/RenderOrderComment";


class OrderComment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Comments: [],
            ID: this.props.navigation.getParam('UserID'),
            OrderID: this.props.navigation.getParam('orderId'),
            Empty: false,
            Count: false,
            Loading: false,
            CommentText: '',
        };
    }

    componentWillMount() {
        this._GetComments()
    }

    componentDidMount() {
        this._StartGettingMessage()
    }

    componentWillUnmount() {
        this.props.navigation.addListener("willBlur", payload => {
            clearInterval(this._StartGettingMessage)
        });
    }


    render() {
        let Dict = Dic[this.props.Language];
        let item = this.props.item;
        return (
            <View style={{flex: 1}}>
                <Header navigation={this.props.navigation}/>
                <ScrollView showsVerticalScrollIndicator={false}
                            style={styles.ChatContainer}
                            ref={ref => this.scrollView = ref}
                            onContentSizeChange={(contentWidth, contentHeight) => {
                                if (this.state.Count == true) {
                                    this.scrollView.scrollToEnd({animated: false});
                                    setTimeout(() => {
                                        this.setState({Count: false})
                                    }, 1000)
                                }
                            }}>
                    <FlatList
                        renderItem={(item) => <RenderOrderComment item={item} Dict={Dict}/>}
                        showsVerticalScrollIndicator={false}
                        data={this.state.Comments}
                        keyExtractor={(index, item) => index.toString()}
                        ListEmptyComponent={() => <ListEmpty
                            EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                            BgColor={'transparent'}/>}
                    />
                </ScrollView>
                <View style={styles.BottomSegment}>
                    <TextInput
                        ref={input => this.textInput = input}
                        value={this.state.CommentText}
                        onChangeText={(CommentText) => this.setState({CommentText})}
                        underlineColorAndroid={"transparent"}
                        multiline={true}
                        placeholder={' '}
                        style={styles.Input}
                    />
                    <TouchableOpacity style={styles.SendButton} onPress={() => this._SendChat(Dict)}>
                        {
                            this.state.Loading ?
                                <ActivityIndicator color={ColorYellow} size={27}/>
                                :
                                <Icon2 name={'send'} color={ColorYellow} size={30}/>
                        }

                    </TouchableOpacity>
                </View>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }

    _StartGettingMessage = () => {
        this._StartGettingMessage = setInterval(() => {
            this._GetComments(this.state.OrderID);
            // console.warn('interval is working')
        }, 15000)
    };

    _GetComments(orderId) {
        Connect.SendPRequest(URLS.Link() + "getchat", {
            orderId: parseInt(orderId),
        }).then(res => {
            if (res) {
                this.setState({Comments: res, Empty: false})
            } else {
                this.setState({Empty: false})
            }
        }).catch((e) => {
            // Alert.alert('خطا', "خطا در دریافت لیست نظرات")
            this.setState({Empty: false})
        });
    }

    _SendChat(Dict) {
        if (this.state.CommentText !== '') {
            this.setState({Empty: true, Loading: true});
            Connect.SendPRequest(URLS.Link() + "sendchat", {
                userId: parseInt(this.props.id),
                message: this.state.CommentText,
                role: 'provider',
                orderId: this.state.OrderID
            }).then(res => {
                if (res) {
                    this._GetComments(this.state.OrderID);
                    this.textInput.clear();
                    this.setState({Empty: false, CommentText: '', Count: true, Loading: false});
                }
            }).catch(() => {
                this.setState({Empty: false, Loading: false});
                this.dropdown.alertWithType('error', '', Dict['global_error']);
            });
        } else {
            this.setState({Empty: false, Loading: false});
        }
    }
}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderComment);