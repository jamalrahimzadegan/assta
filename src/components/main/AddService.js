import React from 'react';
import {
    FlatList,
    View,
    TouchableOpacity,
    Text, Dimensions, AsyncStorage, ActivityIndicator,
} from 'react-native';
import styles from "../../assets/css/AddService";
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {ColorYellow} from "../../assets/css/Styles";
import {Connect} from "../../core/Connect";
import Header from "../Header";
import ListEmpty from "../ListEmpty";
import RenderAllServices from "../Flatlist/RenderAllServices";
import {connect} from "react-redux";
import Dic from "../../core/Dic";
import URLS from "../../core/URLS";

class AddService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ID: '',
            Loading: false,
            Empty: false
        };
        this._Services = [];
        this.AllServices = [];
        this.OnServices = [];
        this._SetKeys = this._SetKeys.bind(this);
        this._DelKey = this._DelKey.bind(this);
        this._SetValue = this._SetValue.bind(this);
    }

    componentWillMount() {
        this.props.navigation.addListener("willFocus", payload => {
            this._GetOnservices();
            this._GetAllServices()
        });
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.MainView}>
                <Header navigation={this.props.navigation}/>
                <View style={{
                    flexDirection: this.props.f_direction,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                    overflow: 'hidden'
                }}>

                    <TouchableOpacity style={[styles.SetButton, {
                        backgroundColor: ColorYellow,
                        width: '35%',
                        height: 60
                    }]}
                                      onPress={() => this._SetServices(Dict)}>
                        {
                            this.state.Loading ?
                                <ActivityIndicator size={32} color={'black'}/>
                                :
                                <Text style={[styles.Comment]}>{Dict['picker_confirm']}</Text>
                        }
                    </TouchableOpacity>
                    <Text style={[styles.Comment, {flexShrink: 1}]}>{Dict['service_desc']}</Text>
                </View>
                <FlatList
                    renderItem={(item, index) => <RenderAllServices OnServices={this.OnServices}
                                                                    Dict={Dict}
                                                                    Language={this.props.Language}
                                                                    item={item.item}
                                                                    index={item.index}
                                                                    _SetKeys={this._SetKeys}
                                                                    _DelKey={this._DelKey}
                                                                    _SetValue={this._SetValue}
                    />}
                    showsVerticalScrollIndicator={false}
                    data={this.AllServices}
                    keyExtractor={(index) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? this.Loading : this.Empty}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}
                               containerStyle={{backgroundColor: "red"}}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }

    //---------------------Getting Orders --------------------------------------------------------------------------------------
    _GetAllServices(Dict) {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link()+"allservices", {})
            .then(res => {
                // console.warn(res)
                if (res) {
                    this.AllServices = res;
                    this.setState({Empty: false});
                    // this.setState({AllServices: res, Empty: false});
                }
            }).catch((e) => {
            this.setState({Empty: false});
            this.dropdown.alertWithType('error', '', Dict['server_error']);
        });
        this.forceUpdate();

    }

    //---------------------Get On services--------------------------------------------------------------------------------------
    _GetOnservices() {
        this._Services = [];
        Connect.SendPRequest(URLS.Link()+"providerservice", {userId: this.props.id})
            .then(res => {
                if (res) {
                    for (let i = 0; i < res.length; i++) {
                        if (!this.OnServices.includes(res[i].serviceId)) {
                            this.OnServices.push(parseInt(res[i].serviceId));
                        }
                        if (!this._Services.includes(res[i].serviceId)) {
                            this._SetKeys(res[i].serviceId);
                        }
                    }
                }
            }).catch((e) => {
        });
        this.forceUpdate();
    }

    //---------------------Set services--------------------------------------------------------------------------------------
    _SetServices(Dict) {
        // console.warn()
        this.setState({Loading: true});
        Connect.SendPRequest(URLS.Link()+"setservice", {
            userId: parseInt(this.props.id),
            services: this._Services,
        }).then(res => {
            // console.warn(res);
            if (res.result) {
                this.setState({Loading: false});
                this.dropdown.alertWithType('success', '', Dict['success_confirm']);
            } else {
                this.setState({Loading: false});
                this.dropdown.alertWithType('error', '', Dict['error_confirm']);
            }
        }).catch((e) => {
            this.setState({Loading: false});
            this.dropdown.alertWithType('error', '', Dict['error_confirm']);
        });
        this.forceUpdate();
    }

    //---------------------Set Keys--------------------------------------------------------------------------------------

    _SetKeys(id) {
        if (!this._Services.includes(id)) {
            this._Services[id] = {"id": id, "discountPercent": 10, "forComExact": 0, "forComPercent": 10};
        }
    }

    //---------------------Set Values--------------------------------------------------------------------------------------
    _SetValue(id, val) {
        this._Services[id] = val;
    }

    //---------------------Deleting Values--------------------------------------------------------------------------------------
    _DelKey(id) {
        this._Services[id] = null;
    }
}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(AddService);