import React from 'react';
import {
    Button,
    FlatList,
    View,
    TouchableOpacity,
    Text, Image, Dimensions, AsyncStorage, ScrollView,
} from 'react-native';
import styles from "../../assets/css/Profile";
import DropdownAlert from 'react-native-dropdownalert';
import Styles from "../../assets/css/Styles";
import {Connect, Media, Slider} from "../../core/Connect";
import ListEmpty from "../ListEmpty";
import Carousel from 'react-native-snap-carousel';
import {NavigationActions, StackActions} from 'react-navigation'
import Icon from "react-native-vector-icons/Ionicons";
import Header from "../Header";

const H = Dimensions.get("window").height
const W = Dimensions.get("window").width
export default class IncreaseMoney extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ID: '',
            Token: '',
            NewOrders: [],
            entries: [],
        };
    }

    componentWillMount() {
        this.props.navigation.addListener("willFocus", payload => {
            AsyncStorage.getItem('id').then((id) => {
                    this.setState({
                            ID: id
                        }, () => null
                        // console.warn('ID: ' + id)
                    );
                    this._GetDetails(id)
                }
            );
        });

    }

    render() {
        return (
            <View style={styles.MainView}>
                <Header navigation={this.props.navigation}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}
                               containerStyle={{backgroundColor: "red"}}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }

    //---------------------Getting Orders --------------------------------------------------------------------------------------
    _GetDetails(ID) {
        Connect.SendPRequest("neworders", {userId: parseInt(ID)}).then(res => {
            // console.warn('new orders: ');
            console.warn(res);
            if (res) {
                this.setState({NewOrders: res});
            }
        }).catch((e) => {
            this.dropdown.alertWithType('error', '', "مشکل در دریافت اطلاعات از سرور ");
        });
    }


}