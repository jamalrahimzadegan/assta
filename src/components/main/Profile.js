import React from 'react';
import {
    ActivityIndicator,
    View,
    TouchableOpacity,
    Text,
    ScrollView,
    TextInput,
} from 'react-native';
import styles from '../../assets/css/Profile';
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {ColorYellow} from '../../assets/css/Styles';
import RNFetchBlob from 'react-native-fetch-blob';
import ImagePicker from 'react-native-image-picker';
import FastImage from 'react-native-fast-image';
import URLS from '../../core/URLS';
import {Connect, Media} from '../../core/Connect';
import Dic from '../../core/Dic';
import {connect} from 'react-redux';
import Header from '../Header';

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            CM: false,
            Rank: '',
            Token: '',
            Name: '',
            Family: '',
            CodeMelli: '',
            FixedNumber: '',
            CompanyName: '',
            Desc: '',
            ProfilePhoto: '',
            NewImageData: '',
            NewImageName: '',
            UploadingPhoto: false,
            Loading: false,
            CompanyAddress: '',
            Lat: '',
            Long: '',
            City: '',
            Province: '',
            ProvinceList: [],
            ProvinceIdList: [],
            CityList: [],
            CityIdList: [],
            CityID: '',
            ProvinceID: '',
        };
    }

    componentWillMount() {
        this._GetDetails();
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={{flex: 1, backgroundColor: ColorYellow}}>
                <Header navigation={this.props.navigation}/>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={styles.MainView}>
                    <View style={styles.ProfilePhotoContainer}>
                        {
                            this.state.UploadingPhoto ?
                                <ActivityIndicator size={50} color={ColorYellow}/>
                                :
                                <TouchableOpacity
                                    style={styles.ProfilePhotoBtn}
                                    onPress={() => this._ProfilePhotoUploud(Dict)}>
                                    {
                                        this.state.ProfilePhoto ?
                                            <FastImage
                                                source={{uri: URLS.Media() + this.state.ProfilePhoto}}
                                                style={[styles.ProfilePhoto, {
                                                    borderBottomLeftRadius: 20,
                                                    borderBottomRightRadius: 20,
                                                    marginHorizontal: 70
                                                }]}
                                            />
                                            :
                                            <FastImage source={require('../../assets/Images/ContractorProfile.png')}
                                                       style={[styles.ProfilePhoto]}
                                            />
                                    }
                                </TouchableOpacity>
                        }
                    </View>
                    <Text style={styles.NameText} numberOfLines={1}>{this.state.Name} {this.state.Family}</Text>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['t_profile_name']}</Text>
                        <TextInput
                            value={this.state.Name}
                            onChangeText={(name) => this.setState({Name: name})}
                            underlineColorAndroid={'transparent'}
                            // keyboardType={"number-pad"}
                            // returnKeyType={"done"}
                            placeholder={this.state.Name ? this.state.Name : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['t_profile_family']}</Text>

                        <TextInput
                            value={this.state.Family}
                            onChangeText={(name) => this.setState({Family: name})}
                            underlineColorAndroid={'transparent'}
                            // keyboardType={"number-pad"}
                            // returnKeyType={"done"}
                            placeholder={this.state.Family ? this.state.Family : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['t_profile_codemeli']}</Text>

                        <TextInput
                            editable={this.state.CM ? false : true}
                            value={this.state.CodeMelli}
                            onChangeText={(name) => this.setState({CodeMelli: name})}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'number-pad'}
                            returnKeyType={'done'}
                            placeholder={this.state.CodeMelli ? this.state.CodeMelli : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['t_profile_company']}</Text>
                        <TextInput
                            value={this.state.CompanyName}
                            onChangeText={(name) => this.setState({CompanyName: name})}
                            underlineColorAndroid={'transparent'}
                            placeholder={this.state.CompanyName ? this.state.CompanyName : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['t_profile_desc']}</Text>
                        <TextInput
                            value={this.state.Desc}
                            onChangeText={(name) => this.setState({Desc: name})}
                            underlineColorAndroid={'transparent'}
                            multiline={true}
                            placeholder={this.state.Desc ? this.state.Desc : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['t_profile_phone']}</Text>
                        <TextInput
                            value={this.state.CompanyAddress}
                            onChangeText={(name) => this.setState({CompanyAddress: name})}
                            underlineColorAndroid={'transparent'}
                            placeholder={this.state.CompanyAddress ? this.state.CompanyAddress : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>


                    <TouchableOpacity style={styles.UpdateButton} onPress={() => this._UpdateProfile(Dict)}>
                        {
                            this.state.Loading ?
                                <ActivityIndicator color={'#fff'} size={32}/>
                                :
                                <Text style={[styles.Label, {color: ColorYellow}]}>{Dict['t_main_confirm']}</Text>
                        }
                    </TouchableOpacity>
                </ScrollView>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={3500}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        );
    }

    //---------------------Getting profile --------------------------------------------------------------------------------------
    _GetDetails() {
        Connect.SendPRequest(URLS.Link() + 'getprofile', {userId: this.props.id})
            .then(res => {
                if (res) {
                    this.setState({
                        CM: res.codeMeli ? true : false, //for edit codeMeli
                        Name: res.fName,
                        Family: res.lName,
                        CodeMelli: res.codeMeli,
                        FixedNumber: res.fixedNumber,
                        CompanyName: res.companyName,
                        Desc: res.desc,
                        ProfilePhoto: res.image,
                        Rank: res.rank,
                        CompanyAddress: res.address,
                        CityID: res.cityId,
                        City: res.CityName,
                        Province: res.ProvinceName,
                        ProvinceID: res.provinceId,
                    });
                }
            }).catch((e) => {
        });
    }

    //----------UpdateProfilePhoto----------------------------------------------------------------------------------------------------------
    _ProfilePhotoUploud(Dict) {
        // console.warn('URLS.Link() is: '+URLS.Media())
        const PersonalPhotoName = (Math.ceil(new Date().getTime())).toString().replace('.', '') + '.jpg';
        ImagePicker.showImagePicker(Connect.ImagePickerOption(
            Dict['imagepicker_Title'],
            Dict['imagepicker_internal'],
            Dict['imagepicker_camera'],
            Dict['imagepicker_cancel'],
        ), (response) => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                this.setState({
                        NewImageData: response.data,
                        UploadingPhoto: true,
                    },
                    () => {
                        RNFetchBlob.fetch('POST', URLS.Link() + 'upload', {
                            Authorization: 'Bearer access-token',
                            otherHeader: 'foo',
                            'Content-Type': 'multipart/form-data',
                        }, [
                            {
                                name: 'file',
                                filename: PersonalPhotoName,
                                type: 'image/jpeg',
                                data: this.state.NewImageData,
                            },
                        ]).then((resp) => {
                            // console.warn(resp);
                            if (resp.respInfo.status == 200) {
                                this._GetDetails()
                                this.setState({
                                    NewImageName: PersonalPhotoName,
                                    UploadingPhoto: false,
                                }, () => {
                                    this._UpdateProfile();
                                });
                            } else {
                                this.setState({UploadingPhoto: false});
                                this.dropdown.alertWithType('error', '', Dict['faild_update']);
                            }
                        }).catch((err) => {
                            this.setState({UploadingPhoto: false});
                            this.dropdown.alertWithType('error', '', Dict['faild_update']);
                        });
                    },
                );
            }
        });
    }

    //---------------------update profile --------------------------------------------------------------------------------------
    _UpdateProfile(Dict) {
        this.setState({Loading: true});
        // console.warn('image Nmae:' + this.state.NewImageName);
        Connect.SendPRequest(URLS.Link() + 'setprofile', {
            userId: parseInt(this.props.id),
            fName: this.state.Name,
            lName: this.state.Family,
            codeMeli: this.state.CodeMelli,
            fixedNumber: this.state.FixedNumber,
            companyName: this.state.CompanyName,
            desc: this.state.Desc,
            image: this.state.NewImageName ? this.state.NewImageName : this.state.ProfilePhoto,
            address: this.state.CompanyAddress,
            cityId: this.state.CityID,
            provinceId: this.state.ProvinceID,
            lat: '0',
            long: '0',
        }).then(res => {
            // console.warn('edit Profile: ');
            // console.warn(res);
            if (res) {
                this.setState({Loading: false});
                this._GetDetails();
                this.props.DrawerUpdateFn(Math.random());
                this.dropdown.alertWithType('success', '', Dict['succsses_update']);
            } else {
                this.setState({Loading: false});
                this.dropdown.alertWithType('error', '', Dict['faild_update']);
            }
        }).catch((e) => {
            this.setState({Loading: false});
            this.dropdown.alertWithType('error', '', Dict['global_error']);
        });
    }


}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        drawer_update: state.drawer_update,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    // console.warn('dispatch: ',dispatch)
    return {
        getId: () => dispatch({type: 'getId'}),
        DrawerUpdateFn: (x) => dispatch({
            type: 'DrawerUpdateFn',
            payload: {dUp: x},

        }),

    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
