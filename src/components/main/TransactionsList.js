import React from 'react';
import {
    FlatList,
    View,
} from 'react-native';
import styles from "../../assets/css/TransactionList";
import DropdownAlert from 'react-native-dropdownalert';
import Styles from "../../assets/css/Styles";
import {Connect} from "../../core/Connect";
import ListEmpty from "../ListEmpty";
import Header from "../Header";
import {connect} from "react-redux";
import Dic from "../../core/Dic";
import RenderFactors from "../Flatlist/RenderFactors";


class TransactionsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Factors: [],
            Empty: false
        };
    }

    componentWillMount() {
        this.props.navigation.addListener("willFocus", payload => {
            this._GetFactors()
        });
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.MainView}>
                <Header navigation={this.props.navigation}/>
                <FlatList
                    style={styles.TansListContainer}
                    renderItem={(item) => <RenderFactors item={item} Language={this.props.Language}
                                                         f_direction={this.props.f_direction} Dict={Dict}
                                                         navigation={this.props.navigation}/>}
                    showsVerticalScrollIndicator={false}
                    // data={[1,2]}
                    data={this.state.Factors}
                    keyExtractor={(index, item) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }


//---------------------Getting Orders --------------------------------------------------------------------------------------
    _GetFactors(ID) {
        this.setState({Empty: true});
        Connect.SendPRequest("factors", {userId: parseInt(ID)})
            .then(res => {
                // console.log('Factors: ');
                // console.warn(res);
                if (res) {
                    this.setState({Factors: res, Empty: false});
                }
            }).catch((e) => {
            this.setState({Empty: false});
        });
    }
}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsList);