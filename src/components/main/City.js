import React from 'react';
import {
    TouchableOpacity,
    View,
    Text, AsyncStorage, BackHandler,
} from 'react-native';
import styles from "../../assets/css/TransactionList";
import DropdownAlert from 'react-native-dropdownalert';
import Styles from "../../assets/css/Styles";
import {Connect} from "../../core/Connect";
import Header from "../Header";
import Picker from "react-native-picker";

export default class City extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Province: [],
            SelctedProvince: '',
        };
        this.CitySelect='';
        this.Confirm='';
        this.Cancel='';
        this.City='';

    }

    componentWillMount() {
        AsyncStorage.multiGet([
            'imagepicker_cancel',
            'picker_confirm',
            'select_city',
            'city_city',
        ]).then((x) => {
            // console.warn('loading: ' + x[7][1])
            this.Cancel = x[0][1];
            this.Confirm = x[1][1];
            this.CitySelect = x[2][1];
            this.City = x[3][1];
            this.forceUpdate();

        });
        this._GetProvince()
    }

    render() {
        return (
            <View style={styles.MainView}>
                <Text style={{ color: '#000', fontSize: 18, margin: 10, textAlign: 'center'}}>{this.CitySelect}</Text>
                <TouchableOpacity style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    padding: 10,
                    backgroundColor: '#01a8ec',
                    borderRadius: 4,
                    width: '50%',
                    alignSelf: 'center',
                    margin: 20
                }} onPress={() => this._SelectCity()}>
                    <Text style={{
                        color: '#000',
                        fontSize: 16
                    }}>{this.state.SelctedProvince ? this.state.SelctedProvince : this.City}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    disabled={this.state.SelctedProvince == ''}
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding: 10,
                        backgroundColor: this.state.SelctedProvince == '' ? '#dadfe1' : '#22313f',
                        borderRadius: 4,
                        width: '80%',
                        alignSelf: 'center',
                        margin: 20
                    }} onPress={() => this._Proceed()}>
                    <Text style={{
                        color: '#fff',
                        fontSize: 16
                    }}>{this.Confirm}</Text>
                </TouchableOpacity>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}
                               containerStyle={{backgroundColor: "red"}}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }


    //---------------------Getting City --------------------------------------------------------------------------------------
    _GetProvince() {
        Connect.SendPRequest("getprovince", {})
            .then((response) => response.json())
            .then((responseJson) => {
                console.warn(responseJson)
                this.setState({Province: responseJson,});
            })
            .catch((error) => {
                // this.dropdown.alertWithType('error', '', "مشکل در دریافت لیست استان ها");
            });

    }

    _SelectCity() {
        let Data = ['-'];
        for (let i = 0; i < this.state.Province.length; i++) {
            Data.push(this.state.Province[i].name)
        }
        Picker.init({
            pickerFontColor: [36, 37, 42, 1],
            pickerToolBarBg: [36, 37, 42, 1],
            pickerConfirmBtnColor: [34, 167, 240, 1],
            pickerCancelBtnColor: [34, 167, 240, 1],
            pickerTitleColor: [228, 241, 254, 1],
            pickerTextEllipsisLen: 15,
            pickerConfirmBtnText: this.Confirm,
            pickerTitleText: ' ',
            pickerCancelBtnText:this.Cancel,
            pickerData: Data.length !== 0 ? Data : [' '],
            onPickerConfirm: city => {
                let I = Data.indexOf(city.toString());
                // console.warn('city: '+city)
                this.setState({
                    SelctedProvince: city.toString(),
                    // Host: this.state.Province[I].url
                }, () => this.forceUpdate());
                AsyncStorage.multiSet([['province', this.state.SelctedProvince]])
                this.forceUpdate();
                // AsyncStorage.multiSet([['province', this.state.SelctedProvince], ['host', this.state.Host]]);
            }
        });
        Picker.show();
    }

    _Proceed() {
        // console.warn('procedd')
        this.props.navigation.replace('Splash')


    }
}