import React from 'react';
import {
    ScrollView,
    AsyncStorage,
    Image,
    View,
    TouchableOpacity,
    Text,
    Modal,
    Alert,
    ActivityIndicator, TextInput,
} from 'react-native';
import styles from "../../assets/css/Details";
import {Connect, Media} from "../../core/Connect";
import Header from "../Header";
import Icon2 from "react-native-vector-icons/Ionicons";
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import FastImage from 'react-native-fast-image';
import {ColorYellow} from "../../assets/css/Styles";
import URLS from "../../core/URLS";
import {connect} from "react-redux";
import Dic from "../../core/Dic";

class Details extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ID: null,
            Details: [],
            Service: [],
            Option: [],
            Loading: false,
            FreeLoading: false,
            Price: '',
            Cost: '',
            modalVisible: false,
            ConfirmEnd: false
        };
    }

    componentWillMount() {
        let Data = this.props.navigation.getParam('Data');
        this._GetDetails(Data.id)
    }

    render() {
        let {Details, Option, Service} = this.state;
        let Dict = Dic[this.props.Language];
        return (
            <View style={{flex: 1, backgroundColor: '#ecf0f1'}}>
                <Header navigation={this.props.navigation}/>
                <ScrollView style={styles.MainView}
                            showsVerticalScrollIndicator={false}>
                    <View style={{flex: 1, marginBottom: 15}}>
                        {Details.image ?
                            <FastImage source={{uri: Details.image ? URLS.Media() + Details.image : null}}
                                       style={[styles.TopImage, {}]}/>
                            :
                            <FastImage source={require('../../assets/Images/Confirm_Logo.png')}
                                       style={[styles.TopImage, {resizeMode: 'cover',}]}/>
                        }
                        <View style={{alignItems: this.props.Language != 'En' ? 'flex-end' : 'flex-start'}}>
                            <View
                                style={[styles.EachRowHeader, {flexDirection: this.props.f_direction}]}>
                                <Text style={[styles.Titr, {}]}>{Dict['Request_runtime']}
                                    <Text numberOfLines={5} style={styles.body}>{Details.reqTime}</Text>
                                </Text>
                            </View>
                            <View
                                style={[styles.EachRowHeader, {flexDirection: this.props.f_direction}]}>
                                <Text style={[styles.Titr, {}]}>{Dict['Request_create_date']}
                                    <Text numberOfLines={5} style={styles.body}>{Details.reqTime}</Text>
                                </Text>
                            </View>
                            <View
                                style={[styles.EachRowHeader, {flexDirection: this.props.f_direction}]}>
                                <Text style={[styles.Titr, {}]}>{Dict['contractor_confirm_time']}
                                    <Text numberOfLines={5} style={styles.body}>{Details.reqTime}</Text>
                                </Text>
                            </View>
                            <View
                                style={[styles.EachRowHeader, {flexDirection: this.props.f_direction}]}>
                                <Text style={[styles.Titr, {}]}>{Dict['customer_name']}
                                    <Text numberOfLines={5} style={styles.body}>{Details.CustomerName}</Text>
                                </Text>
                            </View>
                            <View
                                style={[styles.EachRowHeader, {flexDirection: this.props.f_direction}]}>
                                <Text style={styles.Titr}>{Dict['orderlist_type']}</Text>
                                <Text
                                    style={styles.body}>{this.props.Language == 'Fa' ? Service.title : this.props.Language == 'En' ? Service.titleEn : this.props.Language == 'Ar' ? Service.titleAr : this.props.Language == 'Ku' ? Service.titleKu : null}</Text>
                            </View>
                            <View
                                style={[styles.EachRowHeader, {flexDirection: this.props.f_direction}]}>
                                <Text style={styles.Titr}>{Dict['t_requestdetail_status']}</Text>
                                <Text
                                    style={styles.body}>{this.props.Language == 'Fa' ? Details.status : this.props.Language == 'En' ? Details.statusEn : this.props.Language == 'Ar' ? Details.statusAr : this.props.Language == 'Ku' ? Details.statusKu : null}</Text>
                            </View>
                            <View
                                style={[styles.EachRowHeader, {
                                    flexDirection: this.props.f_direction,
                                    display: Details.cancelTime ? 'none' : Details.confirmTime || Details.sendProviderTime || Details.DoneTime ? 'flex' : 'none'
                                }]}>
                                <Text style={[styles.Titr, {}]}>{Dict['customer_phone']}</Text>
                                <Text style={styles.body}>{Details.tell}</Text>
                            </View>
                            {/*{*/}
                            {/*    !Details.cancelTime > 0 ?*/}
                            {/*        null*/}
                            {/*        :*/}
                            {/*        <View*/}
                            {/*            style={[styles.EachRowHeader, {*/}
                            {/*                display: !Details.cancelTime > 0 ? 'none' : 'flex',*/}
                            {/*                flexDirection: this.props.f_direction,*/}
                            {/*            }]}>*/}
                            {/*            <Text style={[styles.Titr, {}]}>{Dict['cancel_cause']}</Text>*/}
                            {/*            <Text style={styles.body}>{this.CancleCause}</Text>*/}
                            {/*        </View>*/}
                            {/*}*/}
                            <View
                                style={[styles.EachRowHeader, {flexDirection: this.props.f_direction}]}>
                                <Text style={[styles.Titr, {}]}>{Dict['order_desc']}</Text>
                                <Text
                                    style={[styles.body, {textAlign: 'center'}]}>{Option ? (Option.length != 0 ? Option : '-') : '-'}</Text>
                            </View>
                        </View>
                        <View
                            style={[styles.EachRowHeader, {flexDirection: this.props.f_direction}]}>
                            <Text style={[styles.Titr, {}]}>{Dict['city_city']}: </Text>
                            <Text style={styles.body}>{Details.cityId}</Text>
                        </View>
                        <View style={styles.EachRowHeader}>
                            <Text style={[styles.Titr, {}]}>{Dict['orderlist_address']}
                                <Text numberOfLines={5} style={[styles.body, {}]}>
                                    {Details.address}
                                </Text>
                            </Text>
                        </View>
                        <View
                            style={[styles.EachRowHeader, {flexDirection: this.props.f_direction}]}>
                            <Text style={[styles.Titr, {}]}>{Dict['trans_desc']}
                                <Text numberOfLines={5} style={styles.body}>{Details.desc}</Text>
                            </Text>
                        </View>
                        <View
                            style={[styles.EachRowHeader, {
                                flexDirection: this.props.f_direction,
                                display: Details.cancelTime ? 'none' : Details.confirmTime || Details.sendProviderTime || Details.DoneTime ? 'flex' : 'none'
                            }]}>
                            <Text style={[styles.Titr, {}]}>{Dict['customer_position']}</Text>
                        </View>
                        <MapView
                            style={styles.map}
                            zoomControlEnabled={true}
                            region={{
                                latitude: Details.lat ? Number(Details.lat) : 33.6350,
                                longitude: Details.long ? Number(Details.long) : 46.4153,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}
                            initialRegion={{
                                latitude: Details.lat ? Number(Details.lat) : 33.6350,
                                longitude: Details.long ? Number(Details.long) : 46.4153,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}>
                            <Marker
                                image={require('../../assets/Images/MarkerBlue.png')}
                                coordinate={{
                                    latitude: Details.lat ? Number(Details.lat) : 0,
                                    longitude: Details.long ? Number(Details.long) : 0
                                }}
                            />
                        </MapView>
                        <TouchableOpacity
                            style={[styles.DetailButton, {backgroundColor: Details.DoneTime || Details.cancelTime ? '#bdc3c7' : '#22313f'}]}
                            disabled={Details.DoneTime != null || Details.cancelTime != null ? true : false}
                            onPress={() => this._ButtonAction(Details, Dict)}>
                            {
                                this.state.Loading ?
                                    <ActivityIndicator color={'white'} size={29}/>
                                    :
                                    <Text style={[styles.DetailButtonText]}>{this._ButtonText(Details, Dict)}</Text>
                            }
                        </TouchableOpacity>
                    </View>
                    {/*--------------Free order-----------------------------------------------------------------------------------------------*/}
                    <TouchableOpacity
                        style={[styles.DetailButton, {
                            display: Details.DoneTime == null && Details.cancelTime == null && Details.confirmTime ? 'flex' : 'none',
                            marginBottom: 80
                        }]}
                        onPress={() => this._FreeOrder(Details.id)}>
                        {
                            this.state.FreeLoading ?
                                <ActivityIndicator color={'white'} size={29}/>
                                :
                                <Text style={[styles.DetailButtonText,]}>{Dict['free_order']}</Text>}
                    </TouchableOpacity>
                    {
                        Details.DoneTime ?
                            <Text
                                style={[styles.DetailButtonText, {
                                    alignSelf: 'center',
                                    textAlign: 'center',
                                    color: "#000"
                                }]}>{Details.price ? Dict['last_price'] + Connect.FormatNumber(Number(Details.price)) : Dict['last_price'] + '-'}</Text>
                            : null
                    }
                </ScrollView>
                {/*-----------Modal----------------------------------------------------------------------------------------*/}
                <View style={[styles.ModalContainer, {left: this.state.modalVisible ? 0 : 2000}]}>
                    <View style={styles.ModalContainer2}>
                        <View style={styles.ModalContainer3}>
                            {/*<Text style={[styles.DetailButtonText, {color: '#000', marginBottom: 20}]}>*/}
                            {/*    {this.EnterPrice}</Text>*/}
                            <TextInput
                                value={this.state.phone}
                                onChangeText={(Price) => this.setState({Price})}
                                underlineColorAndroid={"transparent"}
                                keyboardType={"number-pad"}
                                returnKeyType={"done"}
                                placeholder={this.price}
                                style={styles.inp}
                            />
                            <TextInput
                                value={this.state.Cost}
                                onChangeText={(Cost) => this.setState({Cost})}
                                underlineColorAndroid={"transparent"}
                                keyboardType={"number-pad"}
                                returnKeyType={"done"}
                                placeholder={this.Cost}
                                style={styles.inp}
                            />
                            <View style={styles.ModalBtnCont}>
                                <TouchableOpacity
                                    onPress={() => this.setState({modalVisible: false})}>
                                    <Text
                                        style={[styles.DetailButtonText, {color: '#000',}]}>{Dict['t_main_cancel']}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => {
                                        if (Number(this.state.Price) >= 5000) {
                                            this._EndJob(Details.id);
                                        } else {
                                            alert(Dict['UnderNormalText']);
                                        }
                                    }}>
                                    <Text
                                        style={[styles.DetailButtonText, {color: '#000',}]}>{Dict['t_main_confirm']}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
                <TouchableOpacity style={styles.ChatButton}
                                  onPress={() => this.props.navigation.navigate('Comment', {
                                      orderId: Details.id,
                                      UserID: this.state.ID
                                  })}>
                    <Icon2 name={"ios-chatboxes"} color={ColorYellow} size={30}/>
                </TouchableOpacity>
            </View>
        )
    }


    //------GetDetails-------------------------------------------------------------------------------------------------------
    _GetDetails(ID) {
        Connect.SendPRequest(URLS.Link() + "orderdetail", {id: parseInt(ID)})
            .then(res => {
                // console.log('orderdetail: ');
                // console.log(res);
                if (res) {
                    this.setState({Details: res.order, Service: res.service}, () => null)
                    // console.warn(res.option[0].answerEn)
                    if (res.option.length != 0) {
                        let x = '';
                        for (let i = 0; i < res.option.length; i++) {
                            if (this.props.Language == 'En') {
                                x = x ? x + ' - ' + res.option[i].answerEn : res.option[i].answerEn
                            } else if (this.props.Language == 'Ar') {
                                x = x + ' - ' + res.option[i].answerAr
                            } else if (this.props.Language == 'Fa') {
                                x = x + ' - ' + res.option[i].answerFa
                            } else if (this.props.Language == 'Ku') {
                                x = x + ' - ' + res.option[i].answerKu
                            }
                        }
                        this.setState({Option: x}, () => null)
                    }
                }
            }).catch((e) => {
        });
    }

    //----------Button text and function--------------------------------------------------------------------------------------------
    _ButtonText(Details, Dict) {
        if (Details.cancelTime != null) {
            return Dict['imagepicker_cancel']
        } else if (Details.DoneTime != null) {
            return Dict['job_ended']
        } else if (Details.sendProviderTime != null) {
            return Dict['t_main_finishwork']
        } else if (Details.confirmTime != null) {
            return Dict['t_main_dispatch']
        } else {
            return Dict['t_main_confirm']
        }
    }

    _ButtonAction(Details, Dict) {
        let ID = Details.id;
        if (Details.cancelTime != null) {
        } else if (Details.DoneTime != null) {
        } else if (Details.sendProviderTime != null) {
            this.setState({modalVisible: true})
            // this._EndJob(ID)
        } else if (Details.confirmTime != null) {
            this._Dispatch(ID, Dict)
        } else {
            this._Confirm(ID, Dict)
        }
    }

    //------Confirm-------------------------------------------------------------------------------------------------------
    _Confirm(ID, Dict) {
        this.setState({Loading: true});
        Connect.SendPRequest(URLS.Link() + "confirm", {id: parseInt(ID), userId: parseInt(this.props.id)})
            .then(res => {
            // console.warn('confirm:' + res)
            if (res) {
                this.setState({Loading: false});
                this._GetDetails(ID);
                Alert.alert('', Dict['confirmed'])
            } else {
                this.setState({Loading: false})
            }
        }).catch((e) => {
            Alert.alert('', Dict['server_error'])
        });
    }

    //------Dispatch-------------------------------------------------------------------------------------------------------
    _Dispatch(ID, Dict) {
        this.setState({Loading: true});
        Connect.SendPRequest(URLS.Link() + "sendprovider", {id: parseInt(ID)})
            .then(res => {
            // console.warn(res)
            if (res) {
                this.setState({Loading: false});
                this._GetDetails(ID);
                this.forceUpdate();
                Alert.alert('', Dict['details_dispatched'])

                // this.dropdown.alertWithType('success', '', "پیمانکار اعزام شد");
            } else {
                this.setState({Loading: false})
            }
        }).catch((e) => {
            Alert.alert('', Dict['server_error'])
            // this.dropdown.alertWithType('error', 'خطا', "مشکل در ارتباط  با سرور، لطفا برای اعزام پیمانکار دوباره تلاش کنید ");
        });
    }

    //------EndJob-------------------------------------------------------------------------------------------------------
    _EndJob(ID, Dict) {
        this.setState({Loading: true, modalVisible: false});
        Connect.SendPRequest(URLS.Link() + "doneorder", {
            id: parseInt(ID),
            price: this.state.Price,
            toolPrice: this.state.Cost,
        }).then(res => {
            // console.warn('end result: ')
            // console.warn(res)
            if (res) {
                // console.warn(res)
                Alert.alert('', res.message)
                // Alert.alert('', this.job_ended)
                this.setState({Loading: false, modalVisible: false});
                this._GetDetails(ID);
                this.forceUpdate();
            } else {
                this.setState({Loading: false, modalVisible: false})
            }
        }).catch((e) => {
            Alert.alert('', this.global_error)
            this.setState({Loading: false, modalVisible: false})
        });

    }

//    --------------Free Order---------------------------
    _FreeOrder(ID, Dict) {
        this.setState({FreeLoading: true});
        Connect.SendPRequest(URLS.Link() + "freeorder", {
            id: parseInt(ID),
        }).then(res => {
            // console.warn(res)
            if (res) {
                // Alert.alert('', this.)
                this.setState({FreeLoading: false});
                this._GetDetails(ID);
                this.forceUpdate();
            } else {
                this.setState({FreeLoading: false})
            }
        }).catch((e) => {
            Alert.alert('', Dict['server_error'])
            this.setState({FreeLoading: false})
        });
    }

}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Details);


