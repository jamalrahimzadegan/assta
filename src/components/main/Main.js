import React from 'react';
import {
    FlatList,
    View,
    TouchableOpacity,
    Text,
    Dimensions,
    AsyncStorage,
    Alert,
} from 'react-native';
import styles from "../../assets/css/Main";
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {ColorIcon} from "../../assets/css/Styles";
import {Connect, Media, MediaSmall, Slider} from "../../core/Connect";
import ListEmpty from "../ListEmpty";
import Carousel from 'react-native-snap-carousel';
import Header from "../Header";
import Icon from "react-native-vector-icons/Ionicons";
import Icon4 from "react-native-vector-icons/Octicons";
import FastImage from 'react-native-fast-image';
import Icon3 from "react-native-vector-icons/MaterialCommunityIcons";
import Dic from "../../core/Dic";
import {connect} from "react-redux";
import RenderNewOrders from "../Flatlist/RenderNewOrders";
import URLS from "../../core/URLS";

const H = Dimensions.get("window").height;
const W = Dimensions.get("window").width;

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Token: '',
            NewOrders: [],
            entries: [],
            Empty: false,
            Cash: false,
            CashMessage: '',
        };
    }

    componentWillMount() {
        AsyncStorage.getItem('token').then((x) => {
            {
                this.setState({Token: x},
                    () => null
                );
            }
        });
        this.props.navigation.addListener("willFocus", payload => {
                this._IsServices(Dic[this.props.Language]);
                this._GetNewOrders()
            }
        );
        this._GetSliders()
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.MainView}>
                <Header navigation={this.props.navigation}/>
                <Carousel
                    autoplay={true}
                    ref={(c) => this._carousel = c}
                    data={this.state.entries}
                    renderItem={this._RenderSlider}
                    sliderWidth={W}
                    containerCustomStyle={{height: 300}}
                    itemWidth={W}/>
                <View style={{marginVertical: 10}}>
                    <View style={styles.MainButtonsContainer}>
                        {/*--------Increase Money------------------------------------------------------------------------------------*/}
                        <TouchableOpacity style={styles.EachButton}
                                          onPress={() => this.props.navigation.navigate('ReCharge', {
                                              cash: this.state.Cash
                                          })}>
                            <Text style={styles.text}>{Dict['recharge_title']}</Text>
                            <Icon name={"md-add"} color={ColorIcon} size={32}/>
                        </TouchableOpacity>
                        {/*--------My orders------------------------------------------------------------------------------------*/}
                        <TouchableOpacity style={styles.EachButton}
                                          onPress={() => this.props.navigation.navigate('MyOrders')}>
                            <Text style={styles.text}>{Dict['my_orders']}</Text>
                            <Icon name={"ios-list"} color={ColorIcon} size={30}/>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.MainButtonsContainer}>
                        {/*--------Factors------------------------------------------------------------------------------------*/}
                        <TouchableOpacity style={styles.EachButton}
                                          onPress={() => this.props.navigation.navigate('TransactionsList')}>
                            <Text style={styles.text}>{Dict['transactions']}</Text>
                            <Icon3 name={"cash-usd"} color={ColorIcon} size={27}/>
                        </TouchableOpacity>
                        {/*--------SERVICES------------------------------------------------------------------------------------*/}
                        <TouchableOpacity style={styles.EachButton}
                                          onPress={() => this.props.navigation.navigate('AddService')}>
                            <Text style={styles.text}>{Dict['services']}</Text>
                            <Icon4 name={"tasklist"} color={ColorIcon} size={27}/>
                        </TouchableOpacity>
                    </View>
                </View>
                {/*------------FlatList-------------------------------------------------------------------------------------*/}
                <Text style={styles.TITR}>{Dict['requests_list']}</Text>
                <FlatList
                    style={styles.NewOrdersContainer}
                    renderItem={(item) => <RenderNewOrders item={item} Dict={Dict} Language={this.props.Language}
                                                           navigation={this.props.navigation}
                                                           f_direction={this.props.f_direction}/>}
                    showsVerticalScrollIndicator={false}
                    data={this.state.NewOrders}
                    keyExtractor={(index, item) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['main_norequest']}
                        BgColor={'transparent'}/>}
                />
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }

//----------------Carousel Render--------------------------------------------------------------------------------------------------------
    _RenderSlider({item, index}) {
        // console.warn(item)
        return (
            <FastImage source={{uri: Slider + item}} style={styles.Slider}/>
        );
    }

//---------------------Getting Orders --------------------------------------------------------------------------------------
    _GetNewOrders(Dict) {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + "neworders", {userId: parseInt(this.props.id)})
            .then(res => {
                if (res.result) {
                    this.setState({NewOrders: res.data, Empty: false, Cash: res.result});
                } else {
                    this.setState({NewOrders: res.data, Empty: false, Cash: res.result, CashMessage: res.message});
                    this.dropdown.alertWithType('error', '', this.props.Language === 'En' ? res.messageEn : this.props.Language === 'Fa' ? res.message : this.props.Language === 'Ar' ? res.messageAr : this.props.Language === 'Ku' ? res.messageKu : null);
                }
            }).catch((e) => {
            this.setState({Empty: false});
            this.dropdown.alertWithType('error', '', Dict['server_error']);
        });
    }

//---------------------Sliders --------------------------------------------------------------------------------------
    _GetSliders() {
        Connect.SendPRequest(URLS.Link() + "slider", {})
            .then(res => {
                if (res) {
                    this.setState({entries: res});
                }
            });
    }

    //---------------------IsServices --------------------------------------------------------------------------------------
    _IsServices(Dict) {
        Connect.SendPRequest(URLS.Link() + "main", {userId: this.props.id})
            .then(res => {
                // console.warn('main');
                // console.warn('On Services: ' + res.serviceCount);
                if (res.serviceCount == 0) {
                    Alert.alert(
                        '',
                        Dict['t_req_noservice'],
                        [
                            {text: this.SelectServices, onPress: () => this.props.navigation.navigate('AddService')},
                        ],
                        {cancelable: false},
                    );
                }
            })
    }
}


//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);