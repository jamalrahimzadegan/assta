import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Linking,
    AsyncStorage,
    Platform,
    Animated, StatusBar,
    Easing,
    Alert
} from 'react-native';
import styles from '../assets/css/Splash';
import {Connect} from "../core/Connect";
import Dic from "../core/Dic";
import {connect} from "react-redux";
import RNRestart from 'react-native-restart';
import URLS from "../core/URLS"; // Import package from node modules

class Splash extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rotate: new Animated.Value(0),
            Update: "none",
            indicatorr: "flex",
            FcmToken: "",
            showLang: "none",
        };
        this.Language = ''
    }

    //------------componentWillMount-------------------------------------------------------------------------------------------------------
    componentWillMount() {
        AsyncStorage.multiGet([
            'language',
            'direction',
            'align',
            'id',
        ]).then((x) => {
            this.props.setLang({
                lang: x[0][1],
                f_direction: x[1][1],
                text_align: x[2][1],
            });
            this.props.setId(x[3][1]);
        });
        this._VersionCheck()
        this._Animate()
    }

    //------------render-------------------------------------------------------------------------------------------------------
    render() {
        let spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '60deg'],
        });
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        return (
            <View style={styles.mainsp}>
                <StatusBar barStyle={"dark-content"} hidden={true}/>
                <View style={{width: "100%", flex: 1, alignItems: "center", justifyContent: "center"}}>
                    {/*------------- logo -------------------------------------------------------------------*/}
                    <Animated.Image source={require('../assets/Images/SplashLogo.png')}
                                    style={[styles.imgf, {
                                        transform: [
                                            {
                                                scaleY: spin
                                            },
                                            {rotateZ: this.state.rotate}
                                        ]
                                    }]}/>
                    {/*------------- Update -------------------------------------------------------------------*/}
                    <View style={[styles.UpdateOuter, {display: this.state.Update}]}>
                        <Text style={styles.SplashUpText}>{Dict['new_version']}</Text>
                        <TouchableOpacity
                            style={[styles.SplashUpdateBtn, {display: Platform.select({android: "flex", ios: "none"})}]}
                            onPress={() => {
                                Linking.openURL(Platform.select({
                                    ios: "e",
                                    android: "https://play.google.com/store/apps/details?id=com.iraqhelperadmin"
                                }));
                            }}>
                            <Text style={styles.SplashUpdateTextBtn}>{Dict['new_version_download']}</Text>
                        </TouchableOpacity>
                    </View>
                    {/*------------- Lang -------------------------------------------------------------------*/}
                    <View style={[styles.languagesView, {display: this.state.showLang}]}>
                        <TouchableOpacity style={styles.langbtns} onPress={() => this._setLang("En")}>
                            <Text style={styles.langtext}>English</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.langbtns} onPress={() => this._setLang("Ar")}>
                            <Text style={styles.langtext}>Arabic</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.langbtns} onPress={() => this._setLang("Ku")}>
                            <Text style={styles.langtext}>Kurdish</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.langbtns} onPress={() => this._setLang("Fa")}>
                            <Text style={styles.langtext}>Persian</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }

    //------------Animation----------------------------------------------------------------------------------------------------------------------------------------
    spinValue = new Animated.Value(0);

    _Animate() {
        Animated.loop(
            Animated.parallel([
                Animated.timing(
                    this.spinValue,
                    {
                        toValue: 1,
                        duration: 1600,
                        easing: Easing.linear,
                        useNativeDriver: true,
                    },
                ), Animated.timing(
                    this.state.rotate,
                    {
                        toValue: 6.28,
                        duration: 1600,
                        easing: Easing.linear,
                        useNativeDriver: true,
                    },
                ),
            ])).start();
        // setTimeout(() => this.spinValue.stopAnimation(({value}) => console.log('Final Value: ' + value)), 4230); // To stop the animation
    }

    //------------set Lang------------------------------------------------------------------------------------------------------------------------------------------
    _setLang(lang) {
        lang === 'En' ? AsyncStorage.multiSet([['align', 'left'], ['language', lang], ['direction', 'row']])
            : AsyncStorage.multiSet([['align', 'right'], ['language', lang], ['direction', 'row-reverse']]);
        this.setState({showLang: 'none', indicatorr: 'flex'});
        this.props.setLang({
            lang: lang,
            f_direction: lang === 'En' ? 'row' : 'row-reverse',
            text_align: lang === 'En' ? 'left' : 'right',
        });
        RNRestart.Restart();
    }

    //------------------Checking Version -------------------------------------------------------------------------------
    _VersionCheck() {
        let Dict = Dic[this.props.Language]
        AsyncStorage.getItem('language').then((lang) => {
            if (lang) {
                Connect.SendPRequest(URLS.Link() + "versionpro", {version: '1.2.9'})
                    .then(versionResp => {
                        if (1) {
                            // if (versionResp.result) {
                            AsyncStorage.setItem('support_phone', versionResp.phone);
                            if (this.props.id) {     //singed UP and logged in
                                this.props.navigation.replace('Main');
                            } else {
                                this.props.navigation.replace('Login');
                            }
                        } else {
                            this.setState({Update: "flex", indicatorr: "none"});
                        }
                    }).catch((e) => {
                    Alert.alert(
                        '',
                        Dict['global_error'],
                        [
                            {
                                text: Dict['t_main_confirm'], onPress: () => {
                                    RNRestart.Restart()
                                }
                            },
                        ],
                        {cancelable: true},
                    );
                });
            } else {
                this.setState({showLang: "flex", indicatorr: "none"});
            }
        })
    }
}

//-----------------------Redux--------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setId: (x) => dispatch({type: 'setId', payload: {id: x}}),
        setLang: (x) => dispatch({
            type: 'setLang',
            payload: {Language: x['lang'], f_direction: x['f_direction'], text_align: x['text_align']},
        }),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash);
