import React from 'react';
import {
    BackAndroid,
    BackHandler,
    View,
    TouchableOpacity,
    Text,
    TextInput,
    ActivityIndicator,
    AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Pincode from 'react-native-code-verification';
import DropdownAlert from 'react-native-dropdownalert';
import firebase from 'react-native-firebase';
import type {Notification, NotificationOpen} from 'react-native-firebase';
import {Connect} from "../../core/Connect";
import Styles from "../../assets/css/Styles";
import styles from "../../assets/css/Login";


export default class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            family: '',
            phone: this.props.navigation.getParam('phone'),
            isUser: false,
            id: 0,
            Token: ''
        };
        // this._handleBackButtonClick = this._handleBackButtonClick.bind(this);
    }

    _loginToken() {
        firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    console.warn('Firebase Token ==', fcmToken);
                    this.props.dispatch(fcmRegister(fcmToken));
                    // this.setState({FcmToken: fcmToken});
                    Connect.SendPRequest("logintoken", {userId: this.state.id, token: fcmToken});
                } else {
                    // user doesn't have a device token yet
                    console.warn('User doesn\'t have a  token yet, Token = ', fcmToken);
                    firebase.messaging().requestPermission()
                    setTimeout(() => {
                        this._loginToken()
                    }, 3000)
                }
            });
    }

    componentWillMount() {
        AsyncStorage.getItem('token').then((x) => this.setState({
                Token: x
            },
            console.warn('token: ' + this.state.Token)))
    }

    componentWillUnmount() {
        AsyncStorage.setItem('Update', 'true');       //for drawer update
    }

    render() {
        return (
            <View style={styles.MainView}>
                <View style={{display: this.state.login1, alignItems: "center",}}>
                    <View style={[styles.MainView1]}>
                        <Icon name={"user"} color={"#d3d3d3"} size={25} style={{marginLeft: 3,}}/>
                        <TextInput
                            value={this.state.name}
                            onChangeText={(name) => this.setState({name})}
                            underlineColorAndroid={"transparent"}
                            // keyboardType={"number-pad"}
                            // returnKeyType={"done"}
                            placeholder={"  نام"}
                            style={styles.inp}
                        />
                    </View>
                    <View style={[styles.MainView1, {marginTop: 15}]}>
                        <Icon name={"user"} color={"#d3d3d3"} size={25} style={{marginLeft: 3,}}/>
                        <TextInput
                            value={this.state.family}
                            onChangeText={(family) => this.setState({family})}
                            underlineColorAndroid={"transparent"}
                            // keyboardType={"number-pad"}
                            // returnKeyType={"done"}
                            placeholder={"  نام خانوادگی"}
                            style={styles.inp}
                        />
                    </View>
                    <TouchableOpacity onPress={() => this._SingUp()} style={styles.loginBtn}
                        // disabled={this.state.family && this.state.name?false:true}
                    >
                        {/*<ActivityIndicator color="#ffffff" style={{display: this.state.btnok}}/>*/}
                        <Text
                            style={[styles.btntext, {}]}>ثبت
                            نام</Text>
                    </TouchableOpacity>
                </View>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}
                               containerStyle={{backgroundColor: "red"}}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }

    _SingUp() {
        if (this.state.family && this.state.name) {

            console.warn('name: ' + this.state.name)
            console.warn('family: ' + this.state.family)
            Connect.SendPRequest("signup", {
                username: this.state.phone,
                lName: this.state.family,
                fName: this.state.name,
                role: "provider",
                token: this.state.Token
            }).then(res => {
                console.warn(res);
                if (res.result) {
                    AsyncStorage.setItem("id", res.id);
                    console.warn('login: ' + res.result);
                    console.warn('id: ' + res.id);
                    this.dropdown.alertWithType('success', 'تبریک', "ثبت نام با موفقیت انجام گرفت");
                    this.props.navigation.replace('Main')
                }
            });
        } else {
            this.dropdown.alertWithType('error', 'خطا', "لطفاً نام و نام خانوادگی را به درستی وارد کنید");

        }
    }


}

