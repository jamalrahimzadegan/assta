import React from 'react';
import {
    ScrollView,
    View,
    TouchableOpacity,
    Text,
    TextInput,
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    Dimensions
} from 'react-native';
import styles from "../../assets/css/Login";
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import Icon1 from 'react-native-vector-icons/MaterialIcons';
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {ColorBlack, ColorYellow} from "../../assets/css/Styles";
import firebase from 'react-native-firebase';
import {Notification, NotificationOpen} from 'react-native-firebase';
import {Connect} from "../../core/Connect";
import CheckBox from "react-native-check-box";
import {connect} from "react-redux";
import Log from "react-native-fetch-blob/utils/log";
import Dic from "../../core/Dic";
import URLS from "../../core/URLS";

const H = Dimensions.get('window').height;
const W = Dimensions.get('window').width;

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login1: "flex",
            login2: "none",
            btnok2: "none",
            btnok: "none",
            codeBack: "",
            userCode: "",
            phone: "",
            isUser: false,
            id: 0,
            fname: "",
            lname: "",
            moaref: "",
            login: '',
            Rules: "",
            ShowRules: false,
            IsChecked: false,

        };
        // this._handleBackButtonClick = this._handleBackButtonClick.bind(this);
        this._TextInp0 = null;
        this._TextInp1 = null;
        this._TextInp2 = null;
        this._TextInp3 = null;
    }

    componentWillMount() {
        this._GetRoles();
        if (this.props.id) this.props.navigation.replace("Main");
    }
    render() {
        let Dict = Dic[this.props.Language];
        return (
            <ScrollView contentContainerStyle={{}} showsVerticalScrollIndicator={false}>

            <View style={styles.MainView}>
                {/*<StatusBar barstyle={"dark-content"} backgroundColor={"#ffffff"}/>*/}
                <View style={{display: this.state.login1, alignItems: "center",}}>
                    <View style={[styles.MainView1]}>
                        <Icon1 name={'phone'} color={ColorBlack} size={25} style={{}}/>
                        <TextInput
                            value={this.state.phone}
                            onChangeText={(phone) => this.setState({phone})}
                            underlineColorAndroid={"transparent"}
                            keyboardType={"number-pad"}
                            returnKeyType={"done"}
                            placeholder={' 07 *********'}
                            style={styles.inp}
                        />
                    </View>
                    <TouchableOpacity onPress={() => this._send(Dict)} style={styles.loginBtn}
                                      disabled={(this.state.btnok === "none" ? false : true)}>
                        <ActivityIndicator color="#ffffff" style={{display: this.state.btnok}}/>
                        <Text
                            style={[styles.btntext, {display: this.state.btnok === 'none' ? 'flex' : 'none'}]}>{Dict['t_login_login']}</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.MainView3, {display: this.state.login2}]}>
                    {/* -- ---- - -- - -- - -- -- - - - - - code------------------------------------ */}
                    <View style={styles.MainView2}>
                        <TextInput
                            value={this.state.userCode}
                            onChangeText={(userCode) => this.setState({userCode})}
                            underlineColorAndroid={"transparent"}
                            keyboardType={"number-pad"}
                            ref={(inp) => this._TextInp0 = inp}
                            returnKeyType={this.state.isUser ? "done" : "next"}
                            placeholder={'' + Dict['t_login_sms'] + ''}
                            style={styles.inp2}
                            blurOnSubmit={false}
                            onSubmitEditing={() => this.state.isUser ? this._TextInp0.blur() : this._TextInp1.focus()}
                        />
                        <Icon2 name={"sms"} color={ColorBlack} size={25} style={{marginLeft: 3,}}/>
                    </View>
                    {/* -- ---- - -- - -- - -- -- - - - - - first name*/}
                    <View style={[styles.MainView2, {display: this.state.isUser ? "none" : "flex"}]}>
                        <TextInput
                            value={this.state.fname}
                            onChangeText={(fname) => this.setState({fname})}
                            underlineColorAndroid={"transparent"}
                            returnKeyType={"next"}
                            ref={(inp) => this._TextInp1 = inp}
                            blurOnSubmit={false}
                            onSubmitEditing={() => this._TextInp2.focus()}
                            placeholder={Dict['t_profile_name']}
                            style={styles.inp2}
                        />
                        <Icon name={"user"} color={ColorBlack} size={25} style={{marginLeft: 3,}}/>
                    </View>
                    {/* -- ---- - -- - -- - -- -- - - - - - last name*/}
                    <View style={[styles.MainView2, {display: this.state.isUser ? "none" : "flex"}]}>
                        <TextInput
                            value={this.state.lname}
                            onChangeText={(lname) => this.setState({lname})}
                            underlineColorAndroid={"transparent"}
                            returnKeyType={"next"}
                            blurOnSubmit={false}
                            onSubmitEditing={() => this._TextInp3.focus()}
                            ref={(inp) => this._TextInp2 = inp}
                            placeholder={Dict['t_profile_family']}
                            style={styles.inp2}
                        />
                        <Icon name={"user"} color={ColorBlack} size={25} style={{marginLeft: 3,}}/>
                    </View>
                    {/* -- ---- - -- - -- - -- -- - - - - - moaref*/}
                    <View style={[styles.MainView2, {display: this.state.isUser ? "none" : "flex"}]}>
                        <TextInput
                            value={this.state.moaref}
                            onChangeText={(moaref) => this.setState({moaref})}
                            underlineColorAndroid={"transparent"}
                            returnKeyType={"done"}
                            style={styles.inp2}
                            ref={(inp) => this._TextInp3 = inp}
                            placeholder={Dict['moraef_code']}
                        />
                        <Icon1 name={"verified-user"} color={ColorBlack} size={25} style={{marginLeft: 3,}}/>
                    </View>
                    <TouchableOpacity style={styles.stlbtnx}
                                      disabled={(this.state.btnok2 === "none" ? false : true)}
                                      onPress={() => this._checkCode(Dict)}>
                        <ActivityIndicator color="#ffffff" style={{display: this.state.btnok2}}/>
                        <Text
                            style={[styles.btntextd, {display: this.state.btnok2 === 'flex' ? 'none' : 'flex'}]}>{Dict['t_main_confirm']}</Text>
                    </TouchableOpacity>
                </View>
                {/*-----------------Rules----------------------------------------------------------------------------------------------*/}
                <View style={
                    [styles.Rules, {
                        top: this.state.ShowRules ? (this.state.login2 == 'flex' && !this.state.isUser ? H / 11 : 3000) : 3000,
                    }]
                }>
                    <ScrollView style={{height: '85%', width: '95%'}}
                                showsVerticalScrollIndicator={false}>
                        <Text style={[styles.btntext, {color: '#333', textAlign: 'right'}]}>
                            {this.state.Rules}
                        </Text>
                        <CheckBox onClick={() => this.setState({IsChecked: !this.state.IsChecked})}
                                  isIndeterminate={false}
                                  style={{}}
                                  isChecked={this.state.IsChecked}
                                  leftText={Dict['agree_rules']}
                                  rightText={Dict['agree_rules']}
                                  leftTextStyle={[styles.AcceptRulesText, {
                                      display: this.props.Language !== 'En' ? 'flex' : 'none'
                                  }]}
                                  rightTextStyle={[styles.AcceptRulesText, {
                                      display: this.props.Language === 'En' ? 'flex' : 'none'
                                  }]}
                                  checkedCheckBoxColor={'#333'}
                        />
                        <TouchableOpacity style={styles.ConfirmButton}
                                          onPress={() => this._ConfirmRules(Dict)}>
                            <Text style={[styles.btntext, {fontSize: 18}]}>{this.Confirm}</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </View>
                {/*-------DropdownAlert---------------------------------------------------------------------------------------------------*/}
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}
                               containerStyle={{backgroundColor: "red"}}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </ScrollView>
        )
    }

    //-------------------------checkCode-----------------------------------------------------------------------
    _checkCode(Dict) {
        if (this.state.userCode == this.state.codeBack) {
            if (this.state.isUser) {
                AsyncStorage.setItem("id", this.state.id.toString());
                AsyncStorage.setItem("phone", this.state.phone);
                this._loginToken();
                this.setState({
                    login1: "flex", login2: "none", phone: "", fname: "", lname: "", userCode: "", ShowRules: false,
                });
                this.props.navigation.replace("Main", {AllServices: this.props.navigation.getParam("AllServices")});
            } else {
                if (this.state.fname.length === 0) {
                    this.dropdown.alertWithType('error', '', this.NonameError);
                } else if (this.state.lname.length === 0) {
                    this.dropdown.alertWithType('error', '', this.NofamilyError);
                } else {
                    console.log("-----------5");
                    this.setState({btnok2: "flex"});
                    Connect.SendPRequest(URLS.Link()+"signup", {
                        username: this.state.phone, role: "provider", fName: this.state.fname,
                        acceptRole: 1,
                        lName: this.state.lname, ...this.state.moaref !== "" ? {moarefCode: this.state.moaref} : null
                    }).then(res => {
                        this.setState({btnok2: "none"});
                        // console.warn("signed up user", res);
                        if (res.hasOwnProperty("id") && res.hasOwnProperty("result") && res.result) {
                            AsyncStorage.setItem("id", res.id.toString());
                            console.log(res.id);
                            AsyncStorage.setItem("phone", this.state.phone);
                            this._loginToken2(res.id);
                            this.setState({
                                login1: "flex",
                                login2: "none",
                                phone: "",
                                fname: "",
                                lname: "",
                                userCode: "",
                            });
                            this.props.navigation.replace("Main", {AllServices: this.props.navigation.getParam("AllServices")});
                        } else {
                            this.dropdown.alertWithType('error', '', Dict['singup_serverError']);
                            this.setState({login1: "flex", login2: "none", ShowRules: true});
                        }
                    });
                }
            }
        } else {
            this.dropdown.alertWithType('error', '', Dict['t_login_wrongcode']);

        }
    }

    //-------------------------Send-----------------------------------------------------------------------
    _send(Dict) {
        console.log({username: this.state.phone});
        if (/^07[\d]{9}$/.test(this.state.phone)) {
            this.setState({btnok: "flex"});
            Connect.SendPRequest(URLS.Link()+"addphone", {username: this.state.phone, role: "provider"})
                .then(res => {
                console.log("add Phone ", res);
                console.warn('code is: '+res.code);
                this.setState({btnok: "flex"});
                if (res.hasOwnProperty("user") && res.user === false) {
                    this.setState({codeBack: res.code, login1: "none", login2: "flex", btnok: "none", ShowRules: true});
                } else if (res.hasOwnProperty("user") && res.user && res.hasOwnProperty("id")) {
                    this.setState({
                        codeBack: res.code,
                        login1: "none",
                        login2: "flex",
                        isUser: true,
                        btnok: "none",
                        id: res.id,
                        ShowRules: true,
                    });
                } else if (res.user && !res.access) {
                    this.setState({btnok: "none"});
                    this.dropdown.alertWithType('error', '', Dict['singup_isUser']);
                } else {
                    this.dropdown.alertWithType('error', '', Dict['global_error']);
                    this.setState({btnok: "none"});
                }
            });
        } else {
            this.dropdown.alertWithType('error', '', Dict['t_login_wrongnumber']);
        }
    }

    //-------------------------GetRoles -----------------------------------------------------------------------
    _GetRoles() {
        Connect.SendPRequest(URLS.Link()+'rules', {})
            .then(res => {
            // console.log('rules: ');
            // console.log(res);
            if (res) {
                AsyncStorage.getItem('language')
                    .then((lang) => {
                    this.setState({Rules: lang === 'En' ? res.valueEn : lang === 'Fa' ? res.value : lang === 'Ar' ?res.valueAr: lang === 'Ku' ? res.valueKu:null})
                });
            }
        }).catch((e) => {});
    }

    //-------------------------ConfirmRules-----------------------------------------------------------------------
    _ConfirmRules() {
        this.state.IsChecked ? this.setState({ShowRules: false}) : this.dropdown.alertWithType('error', '', "لطفا تیک موافقت با قوانین را بزنید.");
    }

    // -------------------------LoginTokens -----------------------------------------------------------------------
    _loginToken() {
        firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    this.props.dispatch(fcmRegister(fcmToken));
                    Connect.SendPRequest("logintoken", {userId: this.state.id, token: fcmToken});
                } else {
                    firebase.messaging().requestPermission()
                    setTimeout(() => {
                        this._loginToken()
                    }, 3000)
                }
            });
    }

    _loginToken2(id) {
        firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    this.props.dispatch(fcmRegister(fcmToken));
                    Connect.SendPRequest("logintoken", {userId: id, token: fcmToken});
                } else {
                    firebase.messaging().requestPermission();
                    setTimeout(() => {
                        this._loginToken2()
                    }, 3000)
                }
            });
    }

}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Log);