import {Component} from "react";
import React from "react";
import {AsyncStorage} from "react-native";
import firebase from "react-native-firebase";



export class Connect extends Component {
    constructor(props) {
        super(props);


    }

    componentWillMount() {
        AsyncStorage.multiGet([]).then((x) => {

            this.forceUpdate()
        })
    }

    static SendPRequest(action, Params = {}) {
        return fetch(action, {
            method: 'POST',Headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }, body: JSON.stringify(Params)
        })
            .then((response) => response.json())
    }

    //--------------GetFcmToken-------------------------------------------------------------------------------------------------
    static GetFcmToken() {
        return firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    AsyncStorage.setItem('token', fcmToken);
                    // AsyncStorage.setItem('token','134');
                    // console.log('Firebase Token ==', fcmToken);
                    this.props.dispatch(fcmRegister(fcmToken));
                } else {
                    // user doesn't have a device token yet
                    // console.warn('User doesn\'t have a  token yet, Token = ', fcmToken);
                    firebase.messaging().requestPermission()
                }
            })
    }

    //--------------FormatNumber-------------------------------------------------------------------------------------------------
    static FormatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    //---------------------------------------------------------------------------------------------------------------
    static ImagePickerOption(a, b, c, d) {
        return {
            // title: 'jamal',
            title: a,
            chooseFromLibraryButtonTitle: b,
            takePhotoButtonTitle: c,
            cancelButtonTitle: d,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        }
    }


}

let Host = 'https://astta.app';
export const Link = `${Host}/admin/mobile/`;
export const Media = `${Host}/upload/`;
export const MediaSmall = `${Host}/upload/smallthumbnail/`;
export const Slider = `${Host}/upload/slider/`;
