import ImagePicker from 'react-native-image-picker';
export const picker = (callback) =>{
    ImagePicker.showImagePicker(options, (response) => {
        if (response.didCancel) {
        }
        else if (response.error) {
        }
        else if (response.customButton) {
        }
        else {
            let source = { uri: response.uri };

            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };

            callback(source , response.data , response.fileName);
        };
    });
};
